<?php

namespace App\Controllers;

use App\Models\BookingAnggotaModel;
use App\Models\BookingKetuaModel;
use App\Models\BookingKontakDaruratModel;

// use App\Models\KuotaModel; //menggunakan namespace pakai use

class Booking extends BaseController
{
    protected $bookinganggotaModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->
    protected $bookingketuaModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->
    protected $bookingkontakdaruratModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->bookinganggotaModel = new BookingAnggotaModel();
        $this->bookingketuaModel = new BookingKetuaModel();
        $this->bookingkontakdaruratModel = new BookingKontakDaruratModel();
    }



    public function index()
    {
        $conn = mysqli_connect('localhost', 'root', '', 'sibookingpenanggungan');
        $today = date('ymd');
        $char = 'PG' . $today;

        $query = mysqli_query($conn, "SELECT max(no_registrasi) as max_id FROM bookingketua WHERE no_registrasi LIKE '{$char}%' ORDER BY no_registrasi DESC LIMIT 1");
        // $query = $db->query('SELECT max(no_registrasi) as kodeTerbesar FROM booking');
        $result = mysqli_fetch_array($query);
        $getId = $result['max_id'];
        $no = substr($getId, -4, 4);
        $no++;

        $no_pendaftaran = $char . sprintf("%04s", $no);

        $tgl_pendaki = $this->request->getPost('tgl_pendakian1');
        $jalur = $this->request->getPost('jalur1');

        $data = [
            'title' => 'Daftar Kelompok',
            'jalur' => $jalur,
            'tgl_pendakian' => $tgl_pendaki,
            'no_pendaftaran' => $no_pendaftaran
        ];
        return view('/pendaftaran/index', $data);
    }


    public function post()
    {
        $conn = mysqli_connect('localhost', 'root', '', 'sibookingpenanggungan');
        $today = date('ymd');
        $char = 'PG' . $today;

        $query = mysqli_query($conn, "SELECT max(no_registrasi) as max_id FROM bookingketua WHERE no_registrasi LIKE '{$char}%' ORDER BY no_registrasi DESC LIMIT 1");
        // $query = $db->query('SELECT max(no_registrasi) as kodeTerbesar FROM booking');
        $result = mysqli_fetch_array($query);
        $getId = $result['max_id'];
        $no = substr($getId, -4, 4);
        $no++;

        $no_pendaftaran = $char . sprintf("%04s", $no);

        $rules = [
            'nama' => 'required',
            'no_identitas' => 'required',
            'tgl_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'kwarganegaraan' => 'required',
            'alamat' => 'required',
            'pekerjaan' => 'required',
            'no_hp' => 'required',
            'nama_darurat1' => 'required',
            'tlp_darurat1' => 'required',
            'hubungan_darurat1' => 'required',
            'nama_darurat2' => 'required',
            'tlp_darurat2' => 'required',
            'hubungan_darurat2' => 'required',



        ];

        if ($this->request->getPost() && $this->validate($rules)) {

            $ketua = [
                'no_registrasi' => $no_pendaftaran,
                'jalur' => $this->request->getPost('jalur'),
                'tgl_pendakian' => $this->request->getPost('tgl_pendakian'),
                'selesai_pendakian' => $this->request->getPost('selesai_pendakian'),
                'nama' => $this->request->getPost('nama'),
                'no_identitas' => $this->request->getPost('no_identitas'),
                'tgl_lahir' => $this->request->getPost('tgl_lahir'),
                'jenis_kelamin' => $this->request->getPost('jenis_kelamin'),
                'kwarganegaraan' => $this->request->getPost('kwarganegaraan'),
                'alamat' => $this->request->getPost('alamat'),
                'pekerjaan' => $this->request->getPost('pekerjaan'),
                'no_hp' => $this->request->getPost('no_hp'),
            ];
            $this->bookingketuaModel->insert($ketua);

            $kontakdarurat = [
                'no_registrasi' => $no_pendaftaran,
                'nama_darurat1' => $this->request->getPost('nama_darurat1'),
                'tlp_darurat1' => $this->request->getPost('tlp_darurat1'),
                'hubungan_darurat1' => $this->request->getPost('hubungan_darurat1'),
                'nama_darurat2' => $this->request->getPost('nama_darurat2'),
                'tlp_darurat2' => $this->request->getPost('tlp_darurat2'),
                'hubungan_darurat2' => $this->request->getPost('hubungan_darurat2'),
            ];
            $this->bookingkontakdaruratModel->insert($kontakdarurat);

            $nama_anggota = $this->request->getPost('nama_anggota');
            $no_tlp = $this->request->getPost('no_tlpanggota');
            $no_identitas = $this->request->getPost('no_identitasanggota');
            $kwarganegaraan = $this->request->getPost('kwarganegaraan_anggota');
            $jenis_kelamin = $this->request->getPost('jenis_kelaminanggota');
            $i = 0;
            if (!empty($nama_anggota)) {
                foreach ($nama_anggota as $items) {
                    $anggota = [
                        'no_registrasi' => $no_pendaftaran,
                        'nama_anggota' => $items,
                        'no_tlpanggota' => $no_tlp[$i],
                        'no_identitasanggota' => $no_identitas[$i],
                        'kwarganegaraan_anggota' => $kwarganegaraan[$i],
                        'jenis_kelaminanggota' => $jenis_kelamin[$i]
                    ];
                    $this->bookinganggotaModel->insert($anggota);
                    $i++;
                }
            }



            $message = [
                'success' => true,
                'notif' => '<div class="alert alert-primary">Data Sudah Ditambahkan</div>'
            ];
        } else {
            $message = [
                'success' => false,

            ];
        }
        return $this->response->setJSON($message);
    }
}
