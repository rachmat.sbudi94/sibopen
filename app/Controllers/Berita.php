<?php

namespace App\Controllers;

use App\Models\BeritaModel; //menggunakan namespace pakai use

class Berita extends BaseController
{
    protected $beritaModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->beritaModel = new BeritaModel();
    }

    public function index()
    {

        $berita = $this->beritaModel->findAll();
        $data = [
            'title' => 'Daftar Berita',
            'berita' => $berita
        ];

        return view('pages/home', $data);
    }

    public function detail($id_berita)
    {
        $data = [
            'title' => 'Detail Berita',
            'berita' => $this->beritaModel->getBerita($id_berita)
        ];
        if (empty($data['berita'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Judul Berita ' . $id_berita . ' tidak ditemukan.');
        }

        return view('pages/detailberita', $data);
    }
}
