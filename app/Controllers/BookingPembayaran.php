<?php

namespace App\Controllers;

use App\Models\BookingPembayaranModel;
use App\Models\BookingKetuaModel;
use App\Models\BookingAnggotaModel;
use App\Models\Admin\DataBookingModel;
use Dompdf\Dompdf;

class BookingPembayaran extends BaseController
{
    protected $bookingpembayaranModel;
    protected $bookingketuaModel;
    protected $bookinganggotaModel;
    protected $databookingModel;
    //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->bookingpembayaranModel = new BookingPembayaranModel();
        $this->bookingketuaModel = new BookingKetuaModel();
        $this->bookinganggotaModel = new BookingAnggotaModel();
        $this->databookingModel = new DataBookingModel();
    }

    public function index()
    {


        $keyword = $this->request->getVar('keyword');

        if ($keyword) {
            $query = $this->bookinganggotaModel->cariAnggota($keyword)->findAll();
        } else {
            $query = $this->bookinganggotaModel;
            $jumlah = "";
        }

        $query2 = $this->bookingketuaModel->getKetua($keyword);

        $jumlah = $this->bookinganggotaModel->cariAnggota($keyword)->countAllResults();
        $jumlahtotal = $jumlah + 1;
        $total =  $jumlahtotal * 10000;



        $data = [

            'title' => 'Detail Pembayaran',
            'anggota' => $query,
            'ketua' => $query2,
            'page' => $this->request->getVar('page') ? $this->request->getVar('page') : 1,
            'jumlah' => $jumlah,
            'jumlahtotal' => $jumlahtotal,
            'stats' => $this->bookingpembayaranModel->getPembayaranStatus($keyword),
            'pembayaran' => $this->bookingketuaModel->getPendaftaran($keyword),
            'validation' => \Config\Services::validation(),
            'total' => $total,

        ];
        if (empty($data['ketua'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Nomor Registrasi ' . $keyword . ' tidak ditemukan.');
        }

        // dd($data);
        return view('/pembayaran/index', $data);
    }

    public function save()
    {
        $fileSampul = $this->request->getFile('bukti_pembayaran');
        $namaSampul = $fileSampul->getRandomName();
        $fileSampul->move('img/bukti', $namaSampul);
        $status = 'Belum Validasi';
        $this->bookingpembayaranModel->insert([
            'no_registrasi' => $this->request->getVar('no_registrasi'),
            'bukti_pembayaran' => $namaSampul,
            'jumlah' =>  $this->request->getVar('totalbayar'),
            'status' => $status,
        ]);
        session()->setFlashdata('pesan', 'Pembayaran sudah dikirim, menunggu proses validasi. Notifikasi dikirim melalui whatsApp');
        return redirect()->to('/Pages');
    }

    public function tiket()
    {
        $no_registrasi = 'PG2206150002';
        $status = 'Sudah Validasi';

        $pembayaran = $this->bookingpembayaranModel->where('no_registrasi', $no_registrasi)
            ->where('status', $status)->findAll();


        $data = [

            'title' => 'Tiket Pendakian',
            'no_registrasi' => $pembayaran,
        ];
        dd($data);
        return redirect()->to('/Pages');
    }

    public function printpdfetiket($keyword)
    {
        // $keyword = $this->request->getVar('no_registrasi');

        if ($keyword) {
            $query = $this->bookinganggotaModel->cariAnggota($keyword)->findAll();
        } else {
            $query = $this->bookinganggotaModel;
        }

        $query2 = $this->bookingketuaModel->getKetua($keyword);


        $data = [

            'anggota' => $query,
            'ketua' => $query2,
            'page' => $this->request->getVar('page') ? $this->request->getVar('page') : 1,
            'stats' => $this->bookingpembayaranModel->getPembayaranStatus($keyword),
            'pembayaran' => $this->bookingketuaModel->getPendaftaran($keyword),
            'validation' => \Config\Services::validation(),


        ];
        $view = view('/admin/printpdf/etiket', $data);
        $dompdf = new Dompdf();

        $dompdf->loadHtml($view);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream("laporan-booking", array("Attachment" => false));
    }
}
