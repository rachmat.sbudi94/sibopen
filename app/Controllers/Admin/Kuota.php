<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

use App\Models\Admin\KuotaModel; //menggunakan namespace pakai use

class Kuota extends BaseController
{
    protected $kuotaModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->kuotaModel = new KuotaModel();
    }

    public function index()
    {
        $list = array();
        $month = date('m');
        $year = 2022;

        for ($d = 1; $d <= 31; $d++) {
            $time = mktime(12, 0, 0, $month, $d, $year);
            if (date('m', $time) == $month) {
                $tanggal = date('Y-m-d', $time);
            }
        }
        $tanggal = date('Y-m-d', $time);
        $data = [
            'title' => 'Kouta Penanggungan',
            'kuota' => $this->kuotaModel->getKuota('aktif'),
            'tanggal' => $tanggal
        ];
        return view('admin/kuota/index', $data);
    }

    public function create()
    {
        $data = [
            'title' => 'Form Tambah Kuota',
            'validation' => \Config\Services::validation()
        ];
        return view('admin/kuota/create', $data);
    }

    public function save()
    {   //validasi data input
        if (!$this->validate([
            //rules default b.inggris
            //'judul' => 'required|is_unique[komik.judul]'
            'id_kuota' => [
                'rules' => 'required|is_unique[kuota.id_kuota]',
                'errors' => [
                    'required' => '{field} id harus di isi.',
                    'is_unique' => '{field} kuota sudah terdaftar'
                ]
            ],
            'jumlah_kuota' => [
                'rules' => 'integer',
                'errors' => [
                    'integer' => 'Harus angka bulat'
                ]
            ]
        ])) {
            //$validation = \Config\Services::validation();
            //dd($validation);
            return redirect()->to('/Admin/Kuota/create')->withInput()->withInput();
        }

        $this->kuotaModel->insert([
            'id_kuota' => $this->request->getVar('id_kuota'),
            'kategori' => $this->request->getVar('kategori'),
            'nama_kuota' => $this->request->getVar('nama_kuota'),
            'jumlah_kuota' => $this->request->getVar('jumlah_kuota'),

        ]);
        session()->setFlashdata('pesan', 'Data berhasil ditambahkan.');
        return redirect()->to('/Admin/Kuota');
    }

    public function edit($id_kuota)
    {
        $data = [
            'title' => 'Form Edit Kuota',
            'validation' => \Config\Services::validation(),
            'kuota' => $this->kuotaModel->getKuota($id_kuota)
        ];
        return view('/admin/kuota/edit', $data);
    }
    public function update($id_kuota)
    {
        //cek judul
        $kuotaLama = $this->kuotaModel->getKuota($this->request->getVar('id_kuota'));
        if ($kuotaLama['id_kuota'] == $this->request->getVar('id_kuota')) {
            $rule_kuota = 'required';
        } else {
            $rule_kuota = 'is_unique[kuota.id_kuota]';
        }
        if (!$this->validate([
            //rules default b.inggris
            //'judul' => 'required|is_unique[komik.judul]'
            'id_kuota' => [
                'rules' =>  $rule_kuota,
                'errors' => [
                    'required' => '{field} Nomor kuota harus di isi.',
                    'is_unique' => '{field} Kuota sudah terdaftar'
                ]
            ],
            'jumlah_kuota' => [
                'rules' => 'integer',
                'errors' => [
                    'integer' => 'Harus bilangan bulat'
                ]
            ]
        ])) {
            //dd($validation);
            return redirect()->to('/Admin/kuota/edit/' . $this->request->getVar('id_kuota'))->withInput();
        }

        $this->kuotaModel->save([
            'id_kuota' => $id_kuota,
            'kategori' => $this->request->getVar('kategori'),
            'nama_kuota' => $this->request->getVar('nama_kuota'),
            'jumlah_kuota' => $this->request->getVar('jumlah_kuota'),
        ]);
        session()->setFlashdata('pesan', 'Data berhasil diubah.');
        return redirect()->to('/Admin/Kuota');
    }

    public function delete($id_kuota)
    {
        //cari gambar berdasarkan id

        //cek jika file gambarnya default.jpg


        //hapus gambar
        //unlink('img/' . $komik['sampul']);

        $this->kuotaModel->delete($id_kuota);
        session()->setFlashdata('pesan', 'Data berhasil dihapus.');
        return redirect()->to('/Admin/kuota');
    }
}
