<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

use App\Models\Admin\TemuanModel; //menggunakan namespace pakai use


class Temuan extends BaseController
{
    protected $temuanModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->temuanModel = new TemuanModel();
    }

    public function index()
    {
        $temuan = $this->temuanModel->findAll();

        $data = [
            'title' => 'Laporan temuan',
            'temuan' => $temuan
        ];

        return view('admin/temuan/index', $data);
    }

    public function create()
    {
        $conn = mysqli_connect('localhost', 'root', '', 'sibookingpenanggungan');
        $today = date('ymd');
        $char = 'TM' . $today;

        $query = mysqli_query($conn, "SELECT max(id_temuan) as max_id FROM temuan WHERE id_temuan LIKE '{$char}%' ORDER BY id_temuan DESC LIMIT 1");
        // $query = $db->query('SELECT max(no_registrasi) as kodeTerbesar FROM booking');
        $result = mysqli_fetch_array($query);
        $getId = $result['max_id'];
        $no = substr($getId, -4, 4);
        $no++;

        $no_temuan = $char . sprintf("%04s", $no);

        $data = [
            'title' => 'Form Tambah temuan',
            'validation' => \Config\Services::validation(),
            'id_temuan' => $no_temuan,
        ];
        return view('admin/temuan/create', $data);
    }

    public function save()
    {   //validasi data input
        if (!$this->validate([
            //rules default b.inggris
            //'judul' => 'required|is_unique[komik.judul]'
            'id_temuan' => [
                'rules' => 'required|is_unique[temuan.id_temuan]',
                'errors' => [
                    'required' => '{field} id harus di isi.',
                    'is_unique' => '{field} temuan sudah terdaftar'
                ]
            ]

        ])) {
            //$validation = \Config\Services::validation();
            //dd($validation);
            return redirect()->to('/temuan/create')->withInput();
        }

        $this->temuanModel->insert([
            'id_temuan' => $this->request->getVar('id_temuan'),
            'nama_barang' => $this->request->getVar('nama_barang'),
            'deskripsi' => $this->request->getVar('deskripsi'),
            'keterangan' => $this->request->getVar('keterangan'),
            'status_temuan' => $this->request->getVar('status_temuan'),

        ]);
        session()->setFlashdata('pesan', 'Data berhasil ditambahkan.');
        return redirect()->to('/Temuan');
    }

    public function edit($id_temuan = '')
    {
        $data = [
            'title' => 'Form Edit temuan',
            'validation' => \Config\Services::validation(),
            'temuan' => $this->temuanModel->gettemuan($id_temuan)
        ];
        return view('/admin/temuan/edit', $data);
    }
    public function update($id_temuan)
    {
        //cek judul
        $temuanLama = $this->temuanModel->gettemuan($this->request->getVar('id_temuan'));
        if ($temuanLama['id_temuan'] == $this->request->getVar('id_temuan')) {
            $rule_temuan = 'required';
        } else {
            $rule_temuan = 'is_unique[temuan.id_temuan]';
        }
        if (!$this->validate([
            //rules default b.inggris
            //'judul' => 'required|is_unique[komik.judul]'
            'id_temuan' => [
                'rules' =>  $rule_temuan,
                'errors' => [
                    'required' => '{field} Nomor temuan harus di isi.',
                    'is_unique' => '{field} temuan sudah terdaftar'
                ]
            ]
        ])) {
            //dd($validation);
            return redirect()->to('/temuan/edit/' . $this->request->getVar('id_temuan'))->withInput();
        }

        $this->temuanModel->save([
            'id_temuan' => $id_temuan,
            'nama_barang' => $this->request->getVar('nama_barang'),
            'deskripsi' => $this->request->getVar('deskripsi'),
            'keterangan' => $this->request->getVar('keterangan'),
            'status_temuan' => $this->request->getVar('status_temuan'),
        ]);
        session()->setFlashdata('pesan', 'Data berhasil diubah.');
        $temuan = $this->temuanModel->findAll();

        $data = [
            'title' => 'Laporan temuan',
            'temuan' => $temuan
        ];

        return view('admin/temuan/index', $data);
    }

    public function delete($id_temuan)
    {
        //cari gambar berdasarkan id

        //cek jika file gambarnya default.jpg


        //hapus gambar
        //unlink('img/' . $komik['sampul']);

        $this->temuanModel->delete($id_temuan);
        session()->setFlashdata('pesan', 'Data berhasil dihapus.');
        return redirect()->to('/temuan');
    }
}
