<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

use App\Models\Admin\AmbilTemuanModel;
use App\Models\Admin\temuanModel;


class AmbilTemuanDitemukan extends BaseController
{
    protected $ambiltemuanModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->
    protected $temuanModel;

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->ambiltemuanModel = new AmbilTemuanModel();
        $this->temuanModel = new temuanModel();
    }

    public function index()
    {
        $temuan = $this->temuanModel->findAll();

        $data = [
            'title' => 'Laporan temuan',
            'temuan' => $temuan
        ];

        return view('admin/temuan/index', $data);
    }

    public function create($id_temuan = '')
    {
        $conn = mysqli_connect('localhost', 'root', '', 'sibookingpenanggungan');
        $today = date('ymd');
        $char = 'AB' . $today;

        $query = mysqli_query($conn, "SELECT max(id_ambiltemuan) as max_id FROM ambiltemuan WHERE id_ambiltemuan LIKE '{$char}%' ORDER BY id_ambiltemuan DESC LIMIT 1");
        // $query = $db->query('SELECT max(no_registrasi) as kodeTerbesar FROM booking');
        $result = mysqli_fetch_array($query);
        $getId = $result['max_id'];
        $no = substr($getId, -4, 4);
        $no++;

        $no_temuan = $char . sprintf("%04s", $no);
        $data = [
            'title' => 'Form Edit Ambil Temuan',
            'validation' => \Config\Services::validation(),
            'temuan' => $this->temuanModel->getTemuan($id_temuan),
            'no_temuan' => $no_temuan,
        ];
        return view('/admin/temuan/diambil', $data);
    }

    public function save($id_temuan)
    {
        $conn = mysqli_connect('localhost', 'root', '', 'sibookingpenanggungan');
        $today = date('ymd');
        $char = 'AB' . $today;

        $query = mysqli_query($conn, "SELECT max(id_ambiltemuan) as max_id FROM ambiltemuan WHERE id_ambiltemuan LIKE '{$char}%' ORDER BY id_ambiltemuan DESC LIMIT 1");
        // $query = $db->query('SELECT max(no_registrasi) as kodeTerbesar FROM booking');
        $result = mysqli_fetch_array($query);
        $getId = $result['max_id'];
        $no = substr($getId, -4, 4);
        $no++;

        $no_temuan = $char . sprintf("%04s", $no);
        //validasi data input
        if (!$this->validate([
            //rules default b.inggris
            //'judul' => 'required|is_unique[komik.judul]'
            'id_ambiltemuan' => [
                'rules' => 'required|is_unique[ambiltemuan.id_ambiltemuan]',
                'errors' => [
                    'required' => '{field} id harus di isi.',
                    'is_unique' => '{field} temuan sudah terdaftar'
                ]
            ]

        ])) {
            //$validation = \Config\Services::validation();
            //dd($validation);
            return redirect()->to('/ambiltemuan/create')->withInput();
        }

        $this->ambiltemuanModel->insert([
            'id_ambiltemuan' => $no_temuan,
            'nama_barang' => $this->request->getVar('nama_barang'),
            'deskripsi' => $this->request->getVar('deskripsi'),
            'keterangan' => $this->request->getVar('keterangan'),
            'status_temuan' => $this->request->getVar('status_temuan'),

        ]);
        $this->temuanModel->delete($id_temuan);
        session()->setFlashdata('pesan', 'Data berhasil ditambahkan.');

        $temuan = $this->temuanModel->findAll();

        $data = [
            'title' => 'Laporan temuan',
            'temuan' => $temuan
        ];

        return view('admin/temuan/index', $data);
    }
    public function delete($id_ambiltemuan)
    {
        //cari gambar berdasarkan id

        //cek jika file gambarnya default.jpg


        //hapus gambar
        //unlink('img/' . $komik['sampul']);

        $this->ambiltemuanModel->delete($id_ambiltemuan);
        session()->setFlashdata('pesan', 'Data berhasil dihapus.');
        return redirect()->to('/temuan');
    }
}
