<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

use App\Models\Admin\KehilanganModel; //menggunakan namespace pakai use


class Kehilangan extends BaseController
{
    protected $kehilanganModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->kehilanganModel = new KehilanganModel();
    }

    public function index()
    {
        $kehilangan = $this->kehilanganModel->findAll();

        $data = [
            'title' => 'Laporan Kehilangan',
            'kehilangan' => $kehilangan
        ];

        return view('admin/kehilangan/index', $data);
    }

    public function create()
    {
        $conn = mysqli_connect('localhost', 'root', '', 'sibookingpenanggungan');
        $today = date('ymd');
        $char = 'KH' . $today;

        $query = mysqli_query($conn, "SELECT max(id_kehilangan) as max_id FROM kehilangan WHERE id_kehilangan LIKE '{$char}%' ORDER BY id_kehilangan DESC LIMIT 1");
        // $query = $db->query('SELECT max(no_registrasi) as kodeTerbesar FROM booking');
        $result = mysqli_fetch_array($query);
        $getId = $result['max_id'];
        $no = substr($getId, -4, 4);
        $no++;

        $no_kehilangan = $char . sprintf("%04s", $no);

        $data = [
            'title' => 'Form Tambah kehilangan',
            'validation' => \Config\Services::validation(),
            'no_kehilangan' => $no_kehilangan,
        ];
        return view('admin/kehilangan/create', $data);
    }

    public function save()
    {   //validasi data input
        if (!$this->validate([
            //rules default b.inggris
            //'judul' => 'required|is_unique[komik.judul]'
            'id_kehilangan' => [
                'rules' => 'required|is_unique[kehilangan.id_kehilangan]',
                'errors' => [
                    'required' => '{field} id harus di isi.',
                    'is_unique' => '{field} kehilangan sudah terdaftar'
                ]
            ]

        ])) {
            //$validation = \Config\Services::validation();
            //dd($validation);
            return redirect()->to('/Kehilangan/create')->withInput();
        }

        $this->kehilanganModel->insert([
            'id_kehilangan' => $this->request->getVar('id_kehilangan'),
            'nama_barang' => $this->request->getVar('nama_barang'),
            'deskripsi' => $this->request->getVar('deskripsi'),
            'no_identitas' => $this->request->getVar('no_identitas'),
            'alamat' => $this->request->getVar('alamat'),
            'no_hp' => $this->request->getVar('no_hp'),
            'status_temuan' => $this->request->getVar('status_temuan'),

        ]);
        session()->setFlashdata('pesan', 'Data berhasil ditambahkan.');
        return redirect()->to('/Kehilangan');
    }

    public function edit($id_kehilangan = '')
    {
        $data = [
            'title' => 'Form Edit kehilangan',
            'validation' => \Config\Services::validation(),
            'kehilangan' => $this->kehilanganModel->getKehilangan($id_kehilangan)
        ];
        return view('/admin/kehilangan/edit', $data);
    }
    public function update($id_kehilangan)
    {
        //cek judul
        $kehilanganLama = $this->kehilanganModel->getKehilangan($this->request->getVar('id_kehilangan'));
        if ($kehilanganLama['id_kehilangan'] == $this->request->getVar('id_kehilangan')) {
            $rule_kehilangan = 'required';
        } else {
            $rule_kehilangan = 'is_unique[kehilangan.id_kehilangan]';
        }
        if (!$this->validate([
            //rules default b.inggris
            //'judul' => 'required|is_unique[komik.judul]'
            'id_kehilangan' => [
                'rules' =>  $rule_kehilangan,
                'errors' => [
                    'required' => '{field} Nomor kehilangan harus di isi.',
                    'is_unique' => '{field} kehilangan sudah terdaftar'
                ]
            ]
        ])) {
            //dd($validation);
            return redirect()->to('/Kehilangan/edit/' . $this->request->getVar('id_kehilangan'))->withInput();
        }

        $this->kehilanganModel->save([
            'id_kehilangan' => $id_kehilangan,
            'nama_barang' => $this->request->getVar('nama_barang'),
            'deskripsi' => $this->request->getVar('deskripsi'),
            'no_identitas' => $this->request->getVar('no_identitas'),
            'alamat' => $this->request->getVar('alamat'),
            'no_hp' => $this->request->getVar('no_hp'),
            'status_temuan' => $this->request->getVar('status_temuan'),
        ]);
        session()->setFlashdata('pesan', 'Data berhasil diubah.');

        $kehilangan = $this->kehilanganModel->findAll();

        $data = [
            'title' => 'Laporan Kehilangan',
            'kehilangan' => $kehilangan
        ];

        return view('admin/kehilangan/index', $data);
    }

    public function delete($id_kehilangan)
    {
        //cari gambar berdasarkan id

        //cek jika file gambarnya default.jpg


        //hapus gambar
        //unlink('img/' . $komik['sampul']);

        $this->kehilanganModel->delete($id_kehilangan);
        session()->setFlashdata('pesan', 'Data berhasil dihapus.');
        return redirect()->to('/Kehilangan');
    }
}
