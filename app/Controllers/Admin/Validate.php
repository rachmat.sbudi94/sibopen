<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

use App\Models\BookingPembayaranModel; //menggunakan namespace pakai use
use App\Models\Admin\ValidateModel; //menggunakan namespace pakai use

class Validate extends BaseController
{
    protected $bookingpembayaranModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->
    protected $validateModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->bookingpembayaranModel = new BookingPembayaranModel();
        $this->validateModel = new ValidateModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Validasi Penanggungan',
            'bookingpembayaran' => $this->bookingpembayaranModel->getPembayaran('Belum Validasi'),

        ];
        return view('admin/validate/index', $data);
    }

    public function update($no_registrasi)
    {
        $pic = 'Komang';
        $status = 'Sudah Validasi';
        $data = [
            'no_registrasi' => $no_registrasi,
            'status' => $status,
            'pic'    => $pic,
        ];
        $this->bookingpembayaranModel->save($data);


        session()->setFlashdata('pesan', 'Pembayaran sudah di Validasi.');
        return redirect()->to('http://localhost:8081/admin/validate/index');
    }
}
