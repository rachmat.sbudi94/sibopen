<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

use App\Models\Admin\SopModel; //menggunakan namespace pakai use

class Sop extends BaseController
{
    protected $sopModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->sopModel = new SopModel();
    }

    public function index()
    {
        $sop = $this->sopModel->findAll();

        $data = [
            'title' => 'Kouta Penanggungan',
            'sop' => $sop
        ];

        return view('admin/sop/index', $data);
    }

    public function edit($id_sop)
    {
        $data = [
            'title' => 'Form Edit Sop',
            'validation' => \Config\Services::validation(),
            'sop' => $this->sopModel->getSop($id_sop)
        ];
        return view('admin/sop/edit', $data);
    }
    public function update($id_sop)
    {
        //cek nama_

        if (!$this->validate([
            //rules default b.inggris
            //'nama_' => 'required|is_unique[komik.nama_]'
            'nama_' => [],
            'sop' => [
                'rules' => 'max_size[sop,2024]',
                'errors' => [
                    'max_size' => 'Ukuran max 2mb',

                ]
            ]
        ])) {
            //dd($validation);
            return redirect()->to('/sop/edit/' . $this->request->getVar('id_sop'))->withInput();
        }

        $fileSop = $this->request->getFile('sop');

        //cek sop, apakah tetap sop lama
        if ($fileSop->getError() == 4) {
            $namaSop = $this->request->getVar('sopLama');
        } else {
            // generate nama file random
            $namaSop = $fileSop->getRandomName();
            //pindahkan sop
            $fileSop->move('pdf', $namaSop);
            //hapus file yang lama
            unlink('pdf/' . $this->request->getVar('sopLama'));
        }

        $this->sopModel->update([
            'id_sop' => $id_sop,
            'nama_sop' => $namaSop
        ]);
        session()->setFlashdata('pesan', 'Data berhasil diubah.');
        return redirect()->to('/sop');
    }
}
