<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

use App\Models\Admin\AmbilTemuanModel;
use App\Models\Admin\KehilanganModel;


class AmbilTemuan extends BaseController
{
    protected $ambiltemuanModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->ambiltemuanModel = new AmbilTemuanModel();
    }

    public function index()
    {

        $data = [
            'title' => 'Laporan Ambil',
            'kehilangan' => $this->ambiltemuanModel->findAll()
        ];

        return view('admin/ambiltemuan/index', $data);
    }
}
