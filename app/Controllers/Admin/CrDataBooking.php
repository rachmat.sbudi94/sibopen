<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

use App\Models\BookingKetuaModel;
use App\Models\BookingAnggotaModel;
use App\Models\BookingPembayaranModel;
use App\Models\Admin\DataPendakianModel;
use DateTime;

class CrDataBooking extends BaseController
{
    protected $bookingketuaModel;
    protected $bookinganggotaModel;
    protected $bookingpembayaranModel;
    protected $datapendakianModel;

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->bookingketuaModel = new BookingKetuaModel();
        $this->bookinganggotaModel = new BookingAnggotaModel();
        $this->bookingpembayaranModel = new BookingPembayaranModel();
        $this->datapendakianModel = new DataPendakianModel();
    }

    public function index()
    {
        $keyword = $this->request->getVar('keyword');

        if ($keyword) {
            $query = $this->bookinganggotaModel->cariAnggota($keyword)->findAll();
        } else {
            $query = $this->bookinganggotaModel;
        }
        $query2 = $this->bookingketuaModel->getKetua($keyword);
        $query3 = $this->bookingpembayaranModel->getStatus($keyword);

        $data = [

            'title' => 'Detail Pembayaran',
            'anggota' => $query,
            'ketua' => $query2,
            'status' => $query3,
            'page' => $this->request->getVar('page') ? $this->request->getVar('page') : 1,

        ];
        // dd($data);
        if (empty($data['ketua'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Nomor Registrasi ' . $keyword . ' tidak ditemukan.');
        }
        return view('/admin/pages/home', $data);
    }
    public function save()
    {
        $date = new DateTime();
        $now = $date->format('Y-m-d H:i:s');
        $this->datapendakianModel->insert([
            'no_registrasi' => $this->request->getVar('no_registrasi'),
            'tgl_cekin' => $now,
            'tgl_cekout' => '',
            'cekout' => 'Belum',

        ]);
        session()->setFlashdata('pesan1', 'Data berhasil ditambahkan.');


        $keyword = $this->request->getVar('keyword');

        if ($keyword) {
            $query = $this->bookinganggotaModel->cariAnggota($keyword)->findAll();
        } else {
            $query = $this->bookinganggotaModel;
        }

        $query2 = $this->bookingketuaModel->getKetua($keyword);

        $data = [

            'title' => 'Detail Pembayaran',
            'anggota' => $query,
            'ketua' => $query2,
            'page' => $this->request->getVar('page') ? $this->request->getVar('page') : 1,

        ];
        if (empty($data['ketua'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Nomor Registrasi ' . $keyword . ' tidak ditemukan.');
        }
        return view('/admin/pages/home', $data);
    }

    public function semuadatapendakian()
    {
        $data_pendakian = $this->datapendakianModel->getSemuaDataPendakian();

        $data = [
            'title' => 'Semua Data Pendakian',
            'datapendakian' => $data_pendakian
        ];
        // dd($data);
        return view('admin/datapendakian/semuadatapendakian', $data);
    }
    public function anggota($no_registrasi)
    {
        $data = [
            'title' => 'Detail Anggota Pendakian',
            'anggota' => $this->bookinganggotaModel->cariAnggota($no_registrasi)->findAll(),
            'ketua' => $this->bookingketuaModel->getKetua($no_registrasi)

        ];
        return view('admin/datapendakian/anggota', $data);
    }
    public function pendakianberlangsung()
    {
        $data_pendakianberlangsung = $this->datapendakianModel->getDataPendakianBerlangsung();

        $data = [
            'title' => 'Pendakian Berlangsung',
            'datapendakian' => $data_pendakianberlangsung
        ];
        // dd($data);
        return view('admin/datapendakian/berlangsung', $data);
    }
    public function updatependakianberlangsung($no_registrasi)
    {
        $date = new DateTime();
        $cekout = 'Sudah';
        $now = $date->format('Y-m-d H:i:s');
        $this->datapendakianModel->save([
            'no_registrasi' => $no_registrasi,
            'cekout' => $cekout,
            'tgl_cekout' => $now,
        ]);

        session()->setFlashdata('pesan', 'Data berhasil diubah.');

        $data_pendakianberlangsung = $this->datapendakianModel->getDataPendakianBerlangsung();

        $data = [
            'title' => 'Pendakian Berlangsung',
            'datapendakian' => $data_pendakianberlangsung
        ];
        // dd($data);
        return view('admin/datapendakian/berlangsung', $data);
    }
}
