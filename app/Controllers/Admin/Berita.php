<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

use App\Models\Admin\BeritaModel; //menggunakan namespace pakai use

class Berita extends BaseController
{
    protected $beritaModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->beritaModel = new BeritaModel();
    }

    public function index()
    {
        $currentPage = $this->request->getVar('page_berita') ? $this->request->getVar('page_berita') : 1;
        // d($this->request->getVar('keyword'));

        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $berita = $this->beritaModel->search($keyword);
        } else {
            $berita = $this->beritaModel;
        }
        $data = [
            'title' => 'Daftar Berita',
            // 'orang' => $this->orangModel->findAll()
            'berita' => $berita->paginate(30, 'berita'),
            'pager' => $this->beritaModel->pager,
            'currentPage' => $currentPage,

        ];

        return view('/admin/berita/index', $data);
    }
    public function detail($id_berita)
    {

        $data = [
            'title' => 'Detail Berita',
            'berita' => $this->beritaModel->getBerita($id_berita)
        ];
        if (empty($data['berita'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Judul Berita ' . $id_berita . ' tidak ditemukan.');
        }

        return view('admin/berita/detail', $data);
    }
    public function create()
    {
        $conn = mysqli_connect('localhost', 'root', '', 'sibookingpenanggungan');
        $today = date('ymd');
        $char = 'BR' . $today;

        $query = mysqli_query($conn, "SELECT max(id_berita) as max_id FROM berita WHERE id_berita LIKE '{$char}%' ORDER BY id_berita DESC LIMIT 1");
        // $query = $db->query('SELECT max(no_registrasi) as kodeTerbesar FROM booking');
        $result = mysqli_fetch_array($query);
        $getId = $result['max_id'];
        $no = substr($getId, -4, 4);
        $no++;

        $no_berita = $char . sprintf("%04s", $no);
        $data = [
            'title' => 'Form Tambah Berita',
            'no_berita' => $no_berita,
            'validation' => \Config\Services::validation()
        ];
        return view('admin/berita/create', $data);
    }

    public function save()
    {   //validasi data input
        if (!$this->validate([
            //rules default b.inggris
            //'judul' => 'required|is_unique[komik.judul]'
            'judul' => [
                'rules' => 'required|is_unique[berita.judul]',
                'errors' => [
                    'required' => '{field} berita harus di isi.',
                    'is_unique' => '{field} berita sudah terdaftar'
                ]
            ],
            'gambar' => [
                'rules' => 'max_size[gambar,1024]|is_image[gambar]|mime_in[gambar,image/jpg,image/jpeg,image/png]',
                'errors' => [
                    'max_size' => 'Ukuran max 1mb',
                    'is_image' => 'Harus file gambar',
                    'mime_in' => 'Harus file gambar'
                ]
            ]
        ])) {
            //$validation = \Config\Services::validation();
            //dd($validation);
            return redirect()->to('/berita/create')->withInput()->withInput();
        }
        //ambil gambar
        $fileGambar = $this->request->getFile('gambar');
        //nama file gambar random
        //$namaSampul = $fileSampul->getRandomName();

        //apakah tidak ada file gambar yang di upload (4 artinya tidk ada file yang diupload)
        if ($fileGambar->getError() == 4) {
            $namaGambar = 'default.jpg';
        } else {
            //nama file gambar random
            $namaGambar = $fileGambar->getRandomName();
            //pindahkan file ke folder img
            $fileGambar->move('imgadmin/imgberita', $namaGambar);
        }
        //coba simpan pakai array dulu dd($this->request->getVar());
        //jika diambil satu" datanya  dd($this->request->getVar('judul'));
        //$slug = url_title($this->request->getVar('judul'), '-', true);
        $this->beritaModel->insert([
            'id_berita' => $this->request->getVar('id_berita'),
            'judul' => $this->request->getVar('judul'),
            'kategori' => $this->request->getVar('kategori'),
            'deskripsi' => $this->request->getVar('deskripsi'),
            'gambar' => $namaGambar
        ]);
        session()->setFlashdata('pesan', 'Data berhasil ditambahkan.');
        return redirect()->to('/Admin/berita');
    }
    public function delete($id_berita)
    {
        //cari gambar berdasarkan id
        $berita = $this->beritaModel->find($id_berita);

        //cek jika file gambarnya default.jpg


        //hapus gambar
        //unlink('img/' . $komik['sampul']);

        $this->beritaModel->delete($id_berita);
        session()->setFlashdata('pesan', 'Data berhasil dihapus.');
        return redirect()->to('/Admin/berita');
    }
    public function edit($id_berita)
    {
        $data = [
            'title' => 'Form Edit Berita',
            'validation' => \Config\Services::validation(),
            'berita' => $this->beritaModel->getBerita($id_berita)
        ];
        return view('admin/berita/edit', $data);
    }
    public function update($id_berita)
    {
        //cek judul
        $beritaLama = $this->beritaModel->getBerita($this->request->getVar('id_berita'));
        if ($beritaLama['judul'] == $this->request->getVar('judul')) {
            $rule_judul = 'required';
        } else {
            $rule_judul = 'is_unique[berita.judul]';
        }
        if (!$this->validate([
            //rules default b.inggris
            //'judul' => 'required|is_unique[komik.judul]'
            'judul' => [
                'rules' =>  $rule_judul,
                'errors' => [
                    'required' => '{field} berita harus di isi.',
                    'is_unique' => '{field} berita sudah terdaftar'
                ]
            ],
            'gambar' => [
                'rules' => 'max_size[gambar,1024]|is_image[gambar]|mime_in[gambar,image/jpg,image/jpeg,image/png]',
                'errors' => [
                    'max_size' => 'Ukuran max 1mb',
                    'is_image' => 'Harus file gambar',
                    'mime_in' => 'Harus file gambar'
                ]
            ]
        ])) {
            //dd($validation);
            return redirect()->to('/Admin/Berita/edit/' . $this->request->getVar('id_berita'))->withInput();
        }

        $fileGambar = $this->request->getFile('gambar');

        //cek gambar, apakah tetap gambar lama
        if ($fileGambar->getError() == 4) {
            $namaGambar = $this->request->getVar('gambarLama');
        } else {
            // generate nama file random
            $namaGambar = $fileGambar->getRandomName();
            //pindahkan gambar
            $fileGambar->move('img', $namaGambar);
            //hapus file yang lama
            unlink('img/' . $this->request->getVar('gambarLama'));
        }

        $this->beritaModel->save([
            'id_berita' => $id_berita,
            'judul' => $this->request->getVar('judul'),
            'kategori' => $this->request->getVar('kategori'),
            'deskripsi' => $this->request->getVar('deskripsi'),
            'gambar' => $namaGambar
        ]);
        session()->setFlashdata('pesan', 'Data berhasil diubah.');
        return redirect()->to('/Admin/berita');
    }
}
