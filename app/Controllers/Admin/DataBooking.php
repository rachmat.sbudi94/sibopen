<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

use App\Models\Admin\DataBookingModel; //menggunakan namespace pakai use
use App\Models\BookingKetuaModel; //menggunakan namespace pakai use
use App\Models\BookingAnggotaModel; //menggunakan namespace pakai use
use App\Models\BookingKontakDaruratModel; //menggunakan namespace pakai use

use Dompdf\Dompdf;

class DataBooking extends BaseController
{
    protected $databookingModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->
    protected $bookingketuaModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->
    protected $bookinganggotaModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->
    protected $bookingkontakdaruratModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->databookingModel = new DataBookingModel();
        $this->bookingketuaModel = new BookingKetuaModel();
        $this->bookinganggotaModel = new BookingAnggotaModel();
        $this->bookingkontakdaruratModel = new BookingKontakDaruratModel();
    }

    public function index()
    {
        $data_booking = $this->bookingketuaModel->findAll();

        $data = [
            'title' => 'Data Booking',
            'databooking' => $data_booking
        ];
        // dd($data);
        return view('admin/databooking/index', $data);
    }
    public function anggota($no_registrasi)
    {
        $data = [
            'title' => 'Detail Anggota Pendakian',
            'anggota' => $this->bookinganggotaModel->cariAnggota($no_registrasi)->findAll(),
            'ketua' => $this->bookingketuaModel->getKetua($no_registrasi),
            'kontakdarurat' => $this->bookingkontakdaruratModel->cariKontak($no_registrasi)->first()

        ];
        return view('admin/databooking/anggota', $data);
    }
    public function periode()
    {
        $tgl_awal = $this->request->getVar('tgl_awal');
        $tgl_akhir = $this->request->getVar('tgl_akhir');

        $data_booking = $this->databookingModel->getPeriodeBooking($tgl_awal, $tgl_akhir);

        $data = [
            'title' => 'Data Booking',
            'databooking' => $data_booking,
            'tgl_awal' => $tgl_awal,
            'tgl_akhir' => $tgl_akhir,
        ];
        // dd($data);
        return view('admin/databooking/index', $data);
    }
    public function printpdfbooking()
    {
        $data_booking = $this->databookingModel->getSimpanan();
        $data = [
            'databooking' => $data_booking
        ];
        $view = view('/admin/printpdf/databooking', $data);
        $dompdf = new Dompdf();
        $dompdf->loadHtml($view);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream("laporan-booking", array("Attachment" => false));
    }
    public function printpdfbookingperiode()
    {
        $tgl_awal = $this->request->getVar('tgl_awal');
        $tgl_akhir = $this->request->getVar('tgl_akhir');

        $data_booking = $this->databookingModel->getPeriodeBooking($tgl_awal, $tgl_akhir);

        $data = [
            'databooking' => $data_booking,
            'tgl_awal' => $tgl_awal,
            'tgl_akhir' => $tgl_akhir,
        ];
        $view = view('/admin/printpdf/databookingperiode', $data);
        $dompdf = new Dompdf();
        $dompdf->loadHtml($view);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream("laporan-booking", array("Attachment" => false));
    }
}
