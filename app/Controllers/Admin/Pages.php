<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

class Pages extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'Dashboard | SI Penanggungan'
        ];

        return view('admin/pages/home', $data);
    }


    public function kuota()
    {
        $data = [
            'title' => 'Kuota | SI Penanggungan'
        ];

        return view('admin/pages/kuota', $data);
    }




    /* Jika memanggil beberapa view
        echo view('layout/header' ,$da);
        echo view('pages/about');
        echo view('layout/footer');
        dan return tidak bisa dipakai karena hanya bisa memanggil 1 method
    */
}
