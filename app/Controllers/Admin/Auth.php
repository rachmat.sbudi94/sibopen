<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

class Auth extends BaseController
{
    public function index()
    {
        return view('admin/auth/login');
    }
    public function register()
    {
        return view('admin/auth/register');
    }
}
