<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

use App\Models\TarifModel; //menggunakan namespace pakai use

class Tarif extends BaseController
{
    protected $tarifModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->tarifModel = new TarifModel();
    }

    public function index()
    {
        $tarif = $this->tarifModel->findAll();

        $data = [
            'title' => 'Tarif Penanggungan',
            'tarif' => $tarif
        ];

        return view('admin/tarif/index', $data);
    }

    public function create()
    {
        $data = [
            'title' => 'Form Tambah Tarif',
            'validation' => \Config\Services::validation()
        ];
        return view('admin/tarif/create', $data);
    }

    public function save()
    {   //validasi data input
        if (!$this->validate([
            //rules default b.inggris
            //'judul' => 'required|is_unique[komik.judul]'
            'id_tarif' => [
                'rules' => 'required|is_unique[tarif.id_tarif]',
                'errors' => [
                    'required' => '{field} id harus di isi.',
                    'is_unique' => '{field} tarif sudah terdaftar'
                ]
            ],
            'harga' => [
                'rules' => 'integer',
                'errors' => [
                    'integer' => 'Harus angka bulat'
                ]
            ]
        ])) {
            //$validation = \Config\Services::validation();
            //dd($validation);
            return redirect()->to('/tarif/create')->withInput()->withInput();
        }

        $this->tarifModel->insert([
            'id_tarif' => $this->request->getVar('id_tarif'),
            'kategori' => $this->request->getVar('kategori'),
            'harga' => $this->request->getVar('harga'),

        ]);
        session()->setFlashdata('pesan', 'Data berhasil ditambahkan.');
        return redirect()->to('/tarif');
    }

    public function edit($id_tarif)
    {
        $data = [
            'title' => 'Form Edit Tarif',
            'validation' => \Config\Services::validation(),
            'tarif' => $this->tarifModel->getTarif($id_tarif)
        ];
        return view('/admin/tarif/edit', $data);
    }
    public function update($id_tarif)
    {
        //cek judul
        $tarifLama = $this->tarifModel->getTarif($this->request->getVar('id_tarif'));
        if ($tarifLama['id_tarif'] == $this->request->getVar('id_tarif')) {
            $rule_tarif = 'required';
        } else {
            $rule_tarif = 'is_unique[tarif.id_tarif]';
        }
        if (!$this->validate([
            //rules default b.inggris
            //'judul' => 'required|is_unique[komik.judul]'
            'id_tarif' => [
                'rules' =>  $rule_tarif,
                'errors' => [
                    'required' => '{field} Nomor tarif harus di isi.',
                    'is_unique' => '{field} Tarif sudah terdaftar'
                ]
            ],
            'harga' => [
                'rules' => 'integer',
                'errors' => [
                    'integer' => 'Harus bilangan bulat'
                ]
            ]
        ])) {
            //dd($validation);
            return redirect()->to('/tarif/edit/' . $this->request->getVar('id_tarif'))->withInput();
        }

        $this->tarifModel->save([
            'id_tarif' => $id_tarif,
            'kategori' => $this->request->getVar('kategori'),
            'harga' => $this->request->getVar('harga'),
        ]);
        session()->setFlashdata('pesan', 'Data berhasil diubah.');
        return redirect()->to('/tarif');
    }

    public function delete($id_tarif)
    {
        //cari gambar berdasarkan id

        //cek jika file gambarnya default.jpg


        //hapus gambar
        //unlink('img/' . $komik['sampul']);

        $this->tarifModel->delete($id_tarif);
        session()->setFlashdata('pesan', 'Data berhasil dihapus.');
        return redirect()->to('/tarif');
    }
}
