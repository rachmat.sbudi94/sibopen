<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;

use App\Models\Admin\DataBookingModel; //menggunakan namespace pakai use

use Dompdf\Dompdf;


class DataBooking extends BaseController
{
    protected $databookingModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->databookingModel = new DataBookingModel();
    }
    // public function index()
    // {
    //     $data_booking = $this->databookingModel->getSimpanan();

    //     $data = [
    //         'title' => 'Data Booking',
    //         'databooking' => $data_booking
    //     ];
    //     // dd($data);
    //     return view('admin/databooking/index', $data);
    // }
    public function printpdfbooking()
    {
        // $data_booking = $this->databookingModel->getSimpanan();

        // $data = [
        //     'title' => 'Data Booking',
        //     'databooking' => $data_booking
        // ];
        // // dd($data);
        // return view('admin/databooking/index', $data);

        // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        $dompdf->loadHtml('hello world');

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream();
    }
}
