<?php

namespace App\Controllers;

use App\Models\BeritaModel;
use App\Models\BookingKetuaModel;

class Pages extends BaseController
{
    protected $beritaModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->
    protected $bookingketuaModel;

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->beritaModel = new BeritaModel();
        $this->bookingketuaModel = new BookingKetuaModel();
    }

    public function index()
    {
        // $berita = $this->beritaModel->findAll();
        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $booking = $this->bookingketuaModel->search($keyword);
        } else {
            $booking = $this->bookingketuaModel;
        }
        $data = [
            'title' => 'Web Gunung Penanggungan',
            // 'berita' => $berita
            'berita' => $this->beritaModel->paginate(4, 'berita'),
            'pager' => $this->beritaModel->pager,
            'booking' => $booking,
        ];

        return view('pages/home', $data);
    }
    public function sop()
    {
        $data = [
            'title' => 'SOP'
        ];

        return view('pages/sop', $data);
    }
    public function pendaftaran()
    {
        $data = [
            'title' => 'Pendaftaran'
        ];
        return view('pages/pendaftaran', $data);
    }
    public function cekbooking()
    {
        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $booking = $this->bookingketuaModel->search($keyword);
        } else {
            $booking = $this->bookingketuaModel;
        }
        $data = [
            'title' => 'Web Gunung Penanggungan',
            // 'berita' => $berita
            'berita' => $this->beritaModel->paginate(4, 'berita'),
            'pager' => $this->beritaModel->pager,
            'booking' => $booking,
        ];

        return view('pembayaran/index', $data);
    }
    public function ceklist()
    {
        $data = [
            'title' => 'Cek List Prasyarat'
        ];

        return view('pendaftaran/ceklist', $data);
    }
}
