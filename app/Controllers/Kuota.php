<?php

namespace App\Controllers;

use App\Models\Admin\KuotaModel;
use App\Models\BookingKetuaModel;

class Kuota extends BaseController
{
    protected $kuotaModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->
    protected $bookingketuaModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->kuotaModel = new KuotaModel();
        $this->bookingketuaModel = new BookingKetuaModel();
    }

    public function index()
    {
        $keyword = 'aktif';
        $kuota = $this->kuotaModel->getHitKuota($keyword);

        $data = [
            'title' => 'Daftar Kuota',
            'kuota' => $kuota,
        ];

        return view('kuota/index', $data);
    }

    public function hitungKuota()
    {
        $keyword = 'aktif';
        $kuota = $this->kuotaModel->getHitKuota($keyword);
        $tgl_pendakian = $this->request->getVar('tgl_pendakian');
        $jalur = $this->request->getVar('jalur');

        if ($tgl_pendakian) {
            $jumlahAnggota = $this->bookingketuaModel->getTanggalAnggota($tgl_pendakian)->countAllResults();
            $jumlahKetua = $this->bookingketuaModel->getTanggalKetua($tgl_pendakian)->countAllResults();
        } else {
            $jumlahAnggota = "";
            $jumlahKetua = "";
        }

        $total = $jumlahAnggota + $jumlahKetua;
        $data = [
            'title' => 'Daftar Kuota',
            'tgl_pendakian' => $tgl_pendakian,
            'jalur' => $jalur,
            'kuota' => $kuota,
            'jumlahpendaki' => $total
        ];
        return view('kuota/index', $data);
    }
}
