<?php

namespace App\Controllers;

use App\Models\PendaftaranModel; //menggunakan namespace pakai use

class PendaftaranKelompok extends BaseController
{
    protected $pendaftaranModel; //agar $komikmodel bisa dipakai  di class lain, namun tambahkan $this->

    public function __construct() //menambahkan construct ,kelasnya dipanggil modelnya ikut dipanggil, semua method bisa pakai
    {
        $this->pendaftaranModel = new PendaftaranModel();
    }

    public function index()
    {
        $booking = $this->pendaftaranModel->findAll();

        $data = [
            'title' => 'Pendaftaran Pendakian',
            'booking' => $booking

        ];
        return view('pendaftaran/index', $data);
    }

    public function create()
    {

        $booking = $this->pendaftaranModel->findAll();
        $no_pendaftaran = $this->pendaftaranModel->noPendaftaran();

        $data = [
            'title' => 'Pendaftaran',
            'booking' => $booking,
            'no_pendaftaran' => $no_pendaftaran,
            'validation' => \Config\Services::validation()

        ];
        return view('pendaftaran/create', $data);
    }

    public function save()
    {
        $no_pendaftaran = $this->pendaftaranModel->noPendaftaran();
        //validasi data input
        if (!$this->validate([
            //rules default b.inggris
            //'judul' => 'required|is_unique[komik.judul]'
            'no_identitas' => [
                'rules' => 'required|is_unique[booking.no_identitas]',
                'errors' => [
                    'required' => '{field} id harus di isi.',
                    'is_unique' => '{field} kuota sudah terdaftar'
                ]
            ],
            'no_hp' => [
                'rules' => 'integer',
                'errors' => [
                    'integer' => 'Harus angka bulat'
                ]
            ]
        ])) {
            //$validation = \Config\Services::validation();
            //dd($validation);
            return redirect()->to('/pendaftaran/create')->withInput();
        }

        $this->pendaftaranModel->insert([
            'no_registrasi' => $no_pendaftaran,
            'tgl_pendakian' => $this->request->getVar('tgl_pendakian'),
            'selesai_pendakian' => $this->request->getVar('selesai_pendakian'),
            'no_identitas' => $this->request->getVar('no_identitas'),
            'jenis_identitas' => $this->request->getVar('jenis_identitas'),
            'nama' => $this->request->getVar('nama'),
            'tgl_lahir' => $this->request->getVar('tgl_lahir'),
            'jenis_kelamin' => $this->request->getVar('jenis_kelamin'),
            'kebangsaan' => $this->request->getVar('kebangsaan'),
            'alamat' => $this->request->getVar('alamat'),
            'pekerjaan' => $this->request->getVar('pekerjaan'),
            'no_hp' => $this->request->getVar('no_hp'),
            'nama_keluarga' => $this->request->getVar('nama_keluarga'),
            'hub_keluarga' => $this->request->getVar('hub_keluarga'),
            'nohp_keluarga' => $this->request->getVar('nohp_keluarga'),
            'pembayaran' => $this->request->getVar('pembayaran'),
            'status' => $this->request->getVar('status'),
        ]);
        session()->setFlashdata('pesan', 'Data berhasil ditambahkan.');
        return redirect()->to('/pembayaran');
    }
}
