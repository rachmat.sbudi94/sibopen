<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>

<div class="container">
    <div class="row">
        <div class="col">
            <a href="/Berita/create" class="btn btn-primary mt-3">Tambah Data Komik</a>
            <?php if (session()->getFlashdata('pesan')) : ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->getFlashdata('pesan'); ?>
                </div>
            <?php endif ?>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">No. Berita</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Kategori</th>
                        <th scope="col">Gambar</th>
                        <th scope="col">Dibuat</th>
                        <th scope="col">Diperbarui</th>
                        <th scope="col">Aksi</th>


                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($berita as $k) : ?>
                        <tr>
                            <th scope="row"><?= $i++; ?></th>
                            <td><?= $k['id_berita']; ?></td>
                            <td><?= $k['judul']; ?></td>
                            <td><?= $k['kategori']; ?></td>
                            <td><?= $k['gambar']; ?></td>
                            <td><?= $k['created_at']; ?></td>
                            <td><?= $k['updated_at']; ?></td>
                            <td>
                                <a href="/berita/<?= $k['id_berita']; ?>" class="btn btn-success">Detail</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>