<?= $this->extend('layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<h2 class="my-2 mt-3 text-center">Form Pendaftaran Pendakian</h2>
<div class="container ">
    <!-- sembunyi , tampil -->
    <!-- <button type="button" class="btn btn-outline-success" onclick="toggleElement()">Tambahkan Kelompok</button> -->

    <form action="/Booking/post" method="post" id="SimpanData" enctype="multipart/form-data">
        <?= csrf_field() ?>
        <!-- <div id="notif"></div> -->
        <div class="card col-md-12 mt-3">
            <h6 class="fst-normal mt-2 text-warning">* Kolom Data Ketua Pendakian dan Data Kontak Darurat Wajib Terisi.</h6>
            <h6 class="fst-normal text-warning">* (Notifikasi via WhatsApp) Pastikan Nomor HP Ketua Anggota sudah benar dan terdaftar WhatsApp. </h6>
            <div class="row justify-content-around mb-3">
                <div class="card justify-content-center col-md-6 ">
                    <div class="card-body">
                        <h5 class="card-header">Data Ketua Pendakian</h5>
                        <div class="col-md-12 ">
                            <input type="hidden" class="form-control " id="no_pendaftaran" name="no_pendaftaran" value="<?= $no_pendaftaran ?>">
                            <div class="form-group row mt-3">
                                <label for="tgl_pendakian" class="col-sm-3 col-form-label">Mulai Pendakian</label>
                                <div class="col-sm-9">
                                    <input type="date" class="form-control" id="tgl_pendakian" name="tgl_pendakian" autofocus value="<?= $tgl_pendakian ?>" readonly>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="selesai_pendakian" class="col-sm-3 col-form-label">Selesai Pendakian</label>
                                <div class="col-sm-9">
                                    <input type="date" class="form-control" id="selesai_pendakian" name="selesai_pendakian" value="<?= old('selesai_pendakian'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jalur" class="col-sm-3 col-form-label">Jalur</label>
                                <div class="col-sm-9">
                                    <select type="text" class="form-select" id="jalur" name="jalur" value="">
                                        <option selected><?= $jalur ?></option>
                                        <option value="Tamiajeng">Tamiajeng</option>
                                        <option value="Jalatunda">Jalatunda</option>
                                        <option value="Wonosunyo">Wonosunyo</option>
                                        <option value="Kedungudi">Kesiman</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="nama" name="nama" value="<?= old('nama'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="no_identitas" class="col-sm-3 col-form-label">No Identitas</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control " id="no_identitas" name="no_identitas" value="<?= old('no_identitas'); ?>">
                                    <div class="invalid-feedback">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="tgl_lahir" class="col-sm-3 col-form-label">Tanggal Lahir</label>
                                <div class="col-sm-9">
                                    <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" value="<?= old('tgl_lahir'); ?>">

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="jenis_kelamin" class="col-sm-3 col-form-label">Jenis Kelamin</label>
                                <div class="col-sm-9">
                                    <select type="text" class="form-select" id="jenis_kelamin" name="jenis_kelamin" value="<?= old('jenis_kelamin'); ?>">
                                        <option selected>Pilih...</option>
                                        <option value="L">Laki-laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="kwarganegaraan" class="col-sm-3 col-form-label">Kewarga negaraan</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="kwarganegaraan" name="kwarganegaraan" value="<?= old('kwarganegaraan'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="alamat" name="alamat" value="<?= old('alamat'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="pekerjaan" class="col-sm-3 col-form-label">Pekerjaan</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="pekerjaan" name="pekerjaan" value="<?= old('pekerjaan'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="no_hp" class="col-sm-3 col-form-label">No. HP</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control no_hp" id="no_hp" name="no_hp">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="card justify-content-center col-md-5">
                    <div class="card-body">
                        <h5 class="card-header">Data Kontak Darurat</h5>
                        <div class="col-md-12 ">
                            <p class="fw-lighter mt-2">*Kontak Darurat 1</p>
                            <div class="form-group row mt-3">
                                <label for="nama_darurat1" class="col-sm-3 col-form-label">Nama Kontak</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="nama_darurat1" name="nama_darurat1" value="<?= old('nama_darurat1'); ?>">
                                    <div class="invalid-feedback">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mt-3">
                                <label for="tlp_darurat1" class="col-sm-3 col-form-label">No. HP</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="tlp_darurat1" name="tlp_darurat1" value="<?= old('tlp_darurat1'); ?>">
                                    <div class="invalid-feedback">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="hubungan_darurat1" class="col-sm-3 col-form-label">Hubungan</label>
                                <div class="col-sm-9">
                                    <select type="text" class="form-select" id="hubungan_darurat1" name="hubungan_darurat1" value="<?= old('hubungan_darurat1'); ?>">
                                        <option selected>Pilih...</option>
                                        <option value="Orang Tua">Orang Tua</option>
                                        <option value="Suami">Suami</option>
                                        <option value="Anak">Anak</option>
                                        <option value="Istri">Istri</option>
                                        <option value="Saudara">Saudara</option>
                                        <option value="Teman">Teman</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <p class="fw-lighter">*Kontak Darurat 2</p>
                            <div class="form-group row mt-3">
                                <label for="nama_darurat2" class="col-sm-3 col-form-label">Nama Kontak</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="nama_darurat2" name="nama_darurat2" value="<?= old('nama_darurat2'); ?>">
                                    <div class="invalid-feedback">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mt-3">
                                <label for="tlp_darurat2" class="col-sm-3 col-form-label">No. HP</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="tlp_darurat2" name="tlp_darurat2" value="<?= old('tlp_darurat2'); ?>">
                                    <div class="invalid-feedback">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="hubungan_darurat2" class="col-sm-3 col-form-label">Hubungan</label>
                                <div class="col-sm-9">
                                    <select type="text" class="form-select" id="hubungan_darurat2" name="hubungan_darurat2" value="<?= old('hubungan_darurat2'); ?>">
                                        <option selected>Pilih...</option>
                                        <option value="Orang Tua">Orang Tua</option>
                                        <option value="Suami">Suami</option>
                                        <option value="Anak">Anak</option>
                                        <option value="Istri">Istri</option>
                                        <option value="Saudara">Saudara</option>
                                        <option value="Teman">Teman</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="mt-2" id="notif"></div>
        <!-- mulai tabel -->
        <div class="card justify-content-center col-md-12 mt-1 ">
            <div class="card-body ">
                <h5 class="card-header">Data Anggota</h5>
                <div class="box-header with-border">
                </div>
                <div class="col-md-12">
                    <h6 class="fst-normal mt-2 text-warning">* Jika tidak mempunyai anggota, hapus isian kolom anggota (Tekan X)</h6>
                </div>
                <div class="col-md-12" style="margin: 10px;">
                    <div class="box box-solid">
                        <!-- <form action="/AnggotaBooking/post" method="post" id="SimpanData" enctype="multipart/form-data"> -->

                        <div class="box-body sembunyi">
                            <table class="table table-striped table-responsive" id="tableLoop">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Nama Anggota</th>
                                        <th>No.Telephone</th>
                                        <th>No.Identitas</th>
                                        <th>Kewarganegaraan</th>
                                        <th>Jenis Kelamin</th>
                                        <th><button class="btn btn-outline-primary btn-block" id="BarisBaru"><b>Tambah</b></button></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>

                            </table>
                        </div>
                        <div class="box-footer">
                            <div class="form-group text-right">
                                <button type="submit" id="btn-booking" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                                <button type="button" class="btn btn-default">Batal</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>
    $(document).ready(function() {
        for (B = 1; B <= 1; B++) {
            Barisbaru();
        }
        $('#BarisBaru').click(function(e) {
            e.preventDefault();
            Barisbaru();
        });

        $("tableLoop tbody").find('input[type=text]').filter(':visible:first').focus();
    });

    function Barisbaru() {

        var Nomor = $("#tableLoop tbody tr").length + 1;
        var Baris = '<tr>';
        Baris += '<td class="text-center">' + Nomor + '</td>';
        Baris += '<td>';
        Baris += '<input type="text" name="nama_anggota[]" class="form-control nama_anggota" placeholder="Nama" required>';
        Baris += '</td>';
        Baris += '<td>';
        Baris += '<input type="text" name="no_tlpanggota[]" class="form-control no_tlpanggota" placeholder="No.Tlp.." required>';
        Baris += '</td>';
        Baris += '<td>';
        Baris += '<input type="text" name="no_identitasanggota[]" class="form-control no_identitasanggota" placeholder="No.Identitas" required>';
        Baris += '</td>';
        Baris += '<td>';
        Baris += '<select type="text" name="kwarganegaraan_anggota[]" class="form-select" id="kwarganegaraan_anggota" required><option selected>Pilih...</option><option value="WNI">WNI</option><option value="WNA">WNA</option></select>';
        Baris += '</td>';
        Baris += '<td>';
        Baris += '<select type="text" name="jenis_kelaminanggota[]" class="form-select" id="jenis_kelaminanggota" required><option selected>Pilih...</option><option value="L">Laki-laki</option><option value="P">Perempuan</option></select>';
        Baris += '</td>';
        Baris += '<td class="text-center">';
        Baris += '<a class="btn btn-sm btn-danger" title="Hapus" id="HapusBaris"><i class="fa fa-times"></i></a>';
        Baris += '</td>';
        Baris += '</tr>';

        $("#tableLoop tbody").append(Baris);
        // $("#tableLoop tbody tr").each(function() {
        //     $(this).find('td:nth-child(2) input').focus();
        // });

    }

    $(document).on('click', '#HapusBaris', function(e) {
        e.preventDefault();
        var Nomor = 1;
        $(this).parent().parent().remove();
        $('tableLoop tbody tr').each(function() {
            $(this).find('td:nth-child(1)').html(Nomor);
            Nomor++;
        });
    });

    $(document).ready(function() {
        $('#SimpanData').submit(function(e) {
            e.preventDefault();
            biodata();
        });
    });

    function biodata() {
        $.ajax({
            url: $("#SimpanData").attr('action'),
            type: 'post',
            cache: false,
            dataType: "json",
            data: $("#SimpanData").serialize(),
            success: function(data) {

                if (data.success == true) {
                    $('#SimpanData').each(function() {
                        this.reset();
                    });
                    $('#notif').fadeIn(800, function() {
                        $("#notif").html(data.notif).fadeOut(5000).delay(800);
                    });
                } else {
                    $('#notif').html('<div class="alert alert-danger"><b>Cek Data</b> Data Gagal Disimpan</div>')
                }
            },

            error: function(error) {
                alert(error);
            }

        });
    }
</script>

<!-- sembunyi, tampil -->
<!-- <script>
    function toggleElement() {
        var x = document.getElementById("tableLoop");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
</script> -->

<?= $this->endSection(); ?>