<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>


<div class="container">
  <div class="row">
    <div class="col">
      <h3 class="text-center p-3">Pendaftaran Pendakian Penanggungan via Tamiajeng </h3>
      <div class="card">
        <div class="card-body">
          <h6>Cek List Persyaratan Pendakian</h6>
          <div class="form-check1 ms-3">
            <input class="form-check-input" type="checkbox" value="A" id="flexCheckDefault1">
            <label class="form-check-label" for="flexCheckDefault">
              Telah membaca SOP
            </label>
          </div>
          <div class="form-check2 ms-3">
            <input class="form-check-input" type="checkbox" value="B" id="flexCheckDefault2">
            <label class="form-check-label" for="flexCheckDefault">
              Tidak akan melakukan vandalisme
            </label>
          </div>
          <div class="form-check3 ms-3">
            <input class="form-check-input" type="checkbox" value="C" id="flexCheckDefault3">
            <label class="form-check-label" for="flexCheckDefault">
              Tidak akan melakukan tindakan asusila
            </label>
          </div>

        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid text-center mb-5 mt-3">
    <!-- <a href="/pendaftaran/create" class="btn btn-primary btn-md">Daftar</a> -->
    <button type="button" class="btn btn-primary btn-md" id="daftarceklist">
      Daftar
    </button>
  </div>

  <script type="text/javascript">
    $("#daftarceklist").click(function() {
      var ceklist1 = $('#flexCheckDefault1').val()
      if (
        $('input#flexCheckDefault1').is(':checked') &&
        $('input#flexCheckDefault2').is(':checked') &&
        $('input#flexCheckDefault3').is(':checked')
      ) {
        window.location.href = "/Kuota";
      } else {
        alert('wajib ceklist')
      }
    });
    // $('#daftarceklist').on('Click', function() {
    //   alert('Test')
    // })
  </script>
  <?= $this->endSection(); ?>