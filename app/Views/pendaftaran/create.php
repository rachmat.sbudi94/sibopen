<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<h2 class="my-5 mt-3 text-center">Form Pendaftaran Pendakian</h2>

<div class="container px-4">
    <div class="row justify-content-center">
        <div class="col-6">
            <h5>Jadwal Pendakian</h5>

            <form action="/Pendaftaran/save" method="post" enctype="multipart/form-data">

                <?= csrf_field(); ?>

                <div class="form-group row ">
                    <label for="text" class="col-sm-3 col-form-label">Mulai Pendakian</label>
                    <div class="col-sm-9">
                        <div class="bootstrap-iso">

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar">
                                    </i>
                                </div>
                                <input type="text" class="form-control" id="date" name="tgl_pendakian" placeholder="DD/MM/YYYY">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="text" class="col-sm-3 col-form-label">Selesai Pendakian</label>
                    <div class="col-sm-9">
                        <div class="bootstrap-iso">

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar">
                                    </i>
                                </div>
                                <input type="text" class="form-control" id="date" name="selesai_pendakian" placeholder="DD/MM/YYYY">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <form action="/Pendaftaran/save" method="post" enctype="multipart/form-data"> -->
                <!-- <?= csrf_field(); ?> -->
                <h5>Data Pendaki</h5>
                <div class="form-group row">
                    <label for="no_identitas" class="col-sm-3 col-form-label">No Identitas</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control <?= ($validation->hasError('no_identitas')) ? 'is-invalid' : ''; ?>" id="no_identitas" name="no_identitas" autofocus value="<?= old('no_identitas'); ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError('no_identitas'); ?>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="jenis_identitas" class="col-sm-3 col-form-label">Jenis Identitas</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="jenis_identitas" name="jenis_identitas" value="<?= old('jenis_identitas'); ?>">
                        <!-- <select class="form-select" id="inputGroupSelect02"> -->
                        <select class="form-select" id="jenis_identitas">
                            <option selected>Pilih...</option>
                            <option value="KTP">KTP</option>
                            <option value="SIM">SIM</option>
                            <option value="Kartu Pelajar">Kartu Pelajar</option>
                            <option value="KITAS">KITAS</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="nama" name="nama" value="<?= old('nama'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="tgl_lahir" class="col-sm-3 col-form-label">Tanggal Lahir</label>
                    <div class="col-sm-9">
                        <div class="bootstrap-iso">

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar">
                                    </i>
                                </div>
                                <input type="text" class="form-control" id="date" name="tgl_lahir" placeholder="DD/MM/YYYY">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="jenis_kelamin" class="col-sm-3 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="jenis_kelamin" name="jenis_kelamin" value="<?= old('jenis_kelamin'); ?>">
                        <select class="form-select" id="inputGroupSelect02">
                            <option selected>Pilih...</option>
                            <option value="L">Laki-laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="kebangsaan" class="col-sm-3 col-form-label">Kebangsaan</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="kebangsaan" name="kebangsaan" value="<?= old('kebangsaan'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="alamat" name="alamat" value="<?= old('alamat'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="pekerjaan" class="col-sm-3 col-form-label">Pekerjaan</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="pekerjaan" name="pekerjaan" value="<?= old('pekerjaan'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="no_hp" class="col-sm-3 col-form-label">No. HP</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="no_hp" name="no_hp" value="<?= old('no_hp'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama_keluarga" class="col-sm-3 col-form-label">Nama Keluarga</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="nama_keluarga" name="nama_keluarga" value="<?= old('nama_keluarga'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="hub_keluarga" class="col-sm-3 col-form-label">Hubungan</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="hub_keluarga" name="hub_keluarga" value="<?= old('hubungan'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nohp_keluarga" class="col-sm-3 col-form-label">No.HP Keluarga</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="nohp_keluarga" name="nohp_keluarga" value="<?= old('nohp_keluarga'); ?>">
                    </div>
                </div>

                <div class="form-group d-grid gap-2 d-md-flex justify-content-md-center">

                    <button type="submit" class="btn btn-outline-primary">Daftar</button>
                    <a class="btn btn-outline-secondary" href="/pages" role="button">Batal</a>

                </div>
            </form>
        </div>
        <h6 class="text-primary text-center">*Pastikan Data yang tercantum sudah benar dan nomor HP terdaftar Whatsapp. Konfirmasi booking akan dikirim melalui Whatsapp</h6>
        <br>

        <h5>Konfirmasi Pendaftaran</h5>
        <h6 class="text-warning fw-bold text-center">Dengan menekan tombol daftar dibawah,maka Anda menyetujui segala persyaratan pendakian di Gunung Penanggungan.</h6>

        <div class="form-group row">
            <img src="/img/hargatiket.jpg" style="max-width:30%;" class="img-thumbnail col-sm-6" alt="...">

            <div class="col-sm-6">
                <h6 class="mt-5">* Harga tiket untuk 1 orang pendaki</h6>

            </div>
        </div>
    </div>
    <form method="post">
        <table>
            <tr>
                <td>Masukkan Nama</td>
                <td>: <input type="text" name="nama"></td>
            </tr>
            <tr>
                <td>Masukkan Alamat</td>
                <td>: <input type="text" name="alamat"></td>
            </tr>
            <tr>
                <td><input type="submit" name="kirim" value="Kirim"></td>
            </tr>
        </table>
    </form>


</div>

<?= $this->endSection(); ?>