<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<!-- memanggil template.php untuk mewariskan method -->

<div class="container">
    <div class="row">
        <div class="card col-md-7 mt-3">
            <h3 class="mt-3 text-center">Kuota Pendakian</h3>
            <div class="card-body justify-content-center">
                <div class="row">
                    <div class="col">
                        <div class="row mt-4">
                            <form action="/Kuota/hitungKuota" method="post" enctype="multipart/form-data">
                                <div class="form-group row">
                                    <label for="tgl_pendakian" class="col-sm-3 col-form-label">Tanggal Pendakian</label>
                                    <div class="col-sm-9">
                                        <div class="bootstrap-iso">
                                            <input type="date" class="form-control" id="tgl_pendakian" name="tgl_pendakian" placeholder="yyyy-mm-dd" value="<?= empty($tgl_pendakian) ? '' : $tgl_pendakian ?>">
                                        </div>
                                        <script>
                                            function myFunction() {
                                                if ($("#tgl_pendakian").val() == null) {
                                                    alert("Pilih tanggal dulu");
                                                }
                                            }
                                        </script>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="jalur" class="col-sm-3 col-form-label">Jalur</label>
                                    <div class="col-sm-8">
                                        <select class="form-select" id="jalur" name="jalur">
                                            <option selected><?= empty($jalur) ? 'Pilih...' : $jalur ?></option>
                                            <option value="Tamiajeng">Tamiajeng</option>
                                            <option value="Jalatunda">Jalatunda</option>
                                            <option value="Kedungudi">Kedungudi</option>
                                            <option value="Wonosunyo">Kesiman</option>
                                        </select>
                                    </div>
                                    <button class="col-sm-1 btn txt-info" href="Kuota/Stats">Cek</button>
                            </form>
                        </div>
                        <div class="form-group row">
                            <label for="jumlahkuota" class="col-sm-3 col-form-label">Jumlah Kuota</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="jumlahkuota" name="jumlahkuota" value="<?= $kuota['jumlah_kuota'] ?>" readonly>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="sisakuota" class="col-sm-3 col-form-label">Sisa Kuota</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="sisakuota" name="sisakuota" value="<?= empty($jumlahpendaki) ? $kuota['jumlah_kuota'] : $kuota['jumlah_kuota'] - $jumlahpendaki ?>" readonly>

                            </div>
                        </div>
                        <form action="/Booking" method="post" enctype="multipart/form-data">
                            <input type="hidden" id="tgl_pendakian1" name="tgl_pendakian1" placeholder="yyyy-mm-dd" value="<?= empty($tgl_pendakian) ? '' : $tgl_pendakian ?>">
                            <input type="hidden" id="jalur1" name="jalur1" value="<?= empty($jalur) ? '' : $jalur ?>">

                            <div class="col text-end">
                                <button type="submit" class="btn btn-outline-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-5 text-center">
        <img src="/img/kuota.jpg" class="rounded mt-3" id="book" width="450" height="300" alt="...">
    </div>

</div>


</div>




<br>
<br>
<br>
<br>
<br>
<br>
<br>
<?= $this->endSection(); ?>