<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Sop</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Halaman</li>
                <li class="breadcrumb-item">Sop</li>
                <li class="breadcrumb-item active">Edit</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <h2 class="my-3">Form Edit Data Komik</h2>

                    <form action="/sop/update/<?= $sop['id_sop']; ?>" method="post" enctype="multipart/form-data">

                        <?= csrf_field(); ?>

                        <div class="form-group row">


                        </div>
                        <div class="form-group row">
                            <label for="text" class="col-sm-2 col-form-label">No. Sop</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control <?= ($validation->hasError('id_sop')) ? 'is-invalid' : ''; ?>" id="id_sop" name="id_sop" autofocus value="<?= (old('id_sop')) ? old('id_sop') : $sop['id_sop'] ?>">
                                <div class="invalid-feedback">
                                    <?= $validation->getError('id_sop'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nama_sop" class="col-sm-2 col-form-label">File Sop</label>

                            <div class="col-sm-8">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input <?= ($validation->hasError('nama_sop')) ? 'is-invalid' : ''; ?>" id="nama_sop" name="nama_sop">
                                    <div class="invalid-feedback">
                                        <?= $validation->getError('nama_sop'); ?>
                                    </div>
                                    <label class="custom-file-label" for="nama_sop"><?= $sop['nama_sop']; ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Edit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main><!-- End #main -->
<?= $this->endSection(); ?>