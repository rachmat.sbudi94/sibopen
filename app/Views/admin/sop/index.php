<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>SOP</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Halaman</li>
                <li class="breadcrumb-item active">SOP</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="row">

                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">No. SOP</th>
                                    <th scope="col">Nama SOP</th>
                                    <th scope="col">Dibuat</th>
                                    <th scope="col">Diperbarui</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                <?php foreach ($sop as $k) : ?>
                                    <tr>
                                        <th scope="row"><?= $i++; ?></th>
                                        <td><?= $k['id_sop']; ?></td>
                                        <td><?= $k['nama_sop']; ?></td>
                                        <td><?= $k['created_at']; ?></td>
                                        <td><?= $k['updated_at']; ?></td>
                                        <td>
                                            <a href="/sop/edit/<?= $k['id_sop']; ?>" class="btn btn-success">Edit</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>
                    <h5>Detail Sop</h5>
                    <div class="card mb-3 my-2" style="max-width: 1500px;">
                        <div class="row g-0">
                            <div class="col-md-12">
                                <div class="card-body">
                                    <h5 class="card-title">Standart Operasional Prosedur</h5>

                                    <iframe src="/pdf/ISMAIL.pdf" width="100%" height="500px"></iframe>
                                    <a src="/pdf/ISMAIL.pdf"></a>





                                    <br><br>
                                    <a href="/sop/edit">Kembali</a>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </section>
</main><!-- End #main -->
<?= $this->endSection(); ?>