<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Temuan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Kehilangan</li>
                <li class="breadcrumb-item">Temuan</li>
                <li class="breadcrumb-item active">Edit</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <h2 class="my-3">Form Edit Temuan</h2>

                    <form action="/Kehilangan/update/<?= $kehilangan['id_kehilangan']; ?>" method="post" enctype="multipart/form-data">

                        <?= csrf_field(); ?>

                        <div class="form-group row">

                            <div class="col-sm-10">
                                <input type="hidden" class="form-control <?= ($validation->hasError('id_erita')) ? 'is-invalid' : ''; ?>" id="id_erita" name="id_erita" autofocus value="<?= old('id_erita'); ?>">
                                <div class="invalid-feedback">
                                    <?= $validation->getError('id_erita'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="text" class="col-sm-4 col-form-label">No. kehilangan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control <?= ($validation->hasError('id_kehilangan')) ? 'is-invalid' : ''; ?>" id="id_kehilangan" name="id_kehilangan" autofocus value="<?= (old('id_kehilangan')) ? old('id_kehilangan') : $kehilangan['id_kehilangan'] ?>">
                                <div class="invalid-feedback">
                                    <?= $validation->getError('id_kehilangan'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nama_barang" class="col-sm-4 col-form-label">Nama Barang</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="nama_barang" name="nama_barang" value="<?= (old('nama_barang')) ? old('nama_barang') : $kehilangan['nama_barang'] ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="deskripsi" class="col-sm-4 col-form-label">Deskripsi</label>
                            <div class="col-sm-8">
                                <textarea type="text" class="form-control" rows="6" id="deskripsi" name="deskripsi"><?= (old('deskripsi')) ? old('deskripsi') : $kehilangan['deskripsi'] ?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="no_identitas" class="col-sm-4 col-form-label">No. Identitas</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="no_identitas" name="no_identitas" value="<?= (old('no_identitas')) ? old('no_identitas') : $kehilangan['no_identitas'] ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alamat" class="col-sm-4 col-form-label">Alamat</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="alamat" name="alamat" value="<?= (old('alamat')) ? old('alamat') : $kehilangan['alamat'] ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="no_hp" class="col-sm-4 col-form-label">No. HP</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="no_hp" name="no_hp" value="<?= (old('no_hp')) ? old('no_hp') : $kehilangan['no_hp'] ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="status_temuan" class="col-sm-4 col-form-label">Status Temuan</label>
                            <div class="col-sm-8">
                                <select class="form-select" onchange="myFunction()" id="status_temuan" name="status_temuan">
                                    <option selected><?= (old('status_temuan')) ? old('status_temuan') : $kehilangan['status_temuan'] ?></option>
                                    <option value="Sudah">Sudah Ditemukan</option>
                                    <option value="Belum">Belum Ditemukan</option>
                                </select>
                            </div>
                        </div>
                        <script>
                            function myFunction() {
                                var x = document.getElementById("editstatus");
                                if ($("#status_temuan").val() == 'Sudah') {
                                    x.value = $("#no_hp").val();
                                } else {
                                    x.value = '';
                                }
                            }
                        </script>
                        <input type="text" id="editstatus" onchange="myFunction()">

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" id="btn-kehilangan" class="btn btn-primary">Edit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main><!-- End #main -->
<?= $this->endSection(); ?>