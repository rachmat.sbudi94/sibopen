<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Temuan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Kehilangan</li>
                <li class="breadcrumb-item">Temuan</li>
                <li class="breadcrumb-item active">Diambil</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <h2 class="my-3">Form Temuan Diambil</h2>

                    <form action="/Admin/AmbilTemuanKehilangan/save/<?= $kehilangan['id_kehilangan']; ?>" method="post" enctype="multipart/form-data">

                        <?= csrf_field(); ?>

                        <div class="form-group row">

                            <div class="col-sm-10">
                                <input type="hidden" class="form-control <?= ($validation->hasError('id_erita')) ? 'is-invalid' : ''; ?>" id="id_erita" name="id_erita" autofocus value="<?= old('id_erita'); ?>">
                                <div class="invalid-feedback">
                                    <?= $validation->getError('id_erita'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="text" class="col-sm-4 col-form-label">No. Ambil Temuan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control " id="id_ambiltemuan" name="id_ambiltemuan" autofocus value="<?= $no_temuan ?>">
                                <div class="invalid-feedback">
                                    <?= $validation->getError('id_ambiltemuan'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nama_barang" class="col-sm-4 col-form-label">Nama Barang</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="nama_barang" name="nama_barang" value="<?= (old('nama_barang')) ? old('nama_barang') : $kehilangan['nama_barang'] ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="deskripsi" class="col-sm-4 col-form-label">Deskripsi</label>
                            <div class="col-sm-8">
                                <textarea type="text" class="form-control" rows="6" id="deskripsi" name="deskripsi"><?= $kehilangan['deskripsi'] ?></textarea>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="no_identitas" class="col-sm-4 col-form-label">No. Identitas</label>
                            <div class="col-sm-8 mt-2">
                                <input type="text" class="form-control" id="no_identitas" name="no_identitas" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alamat" class="col-sm-4 col-form-label">Alamat</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="alamat" name="alamat" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="no_hp" class="col-sm-4 col-form-label">No. HP</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="no_hp" name="no_hp" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="keterangan" class="col-sm-4 col-form-label">Keterangan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="keterangan" name="keterangan" value="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main><!-- End #main -->
<?= $this->endSection(); ?>