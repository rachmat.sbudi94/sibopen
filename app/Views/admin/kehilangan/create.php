<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Temuan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Halaman</li>
                <li class="breadcrumb-item ">Kehilangan</li>
                <li class="breadcrumb-item active">Tambah Temuan</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <h2 class="my-3">Form Tambah Temuan</h2>

                    <form action="/Admin/Kehilangan/save" method="post" enctype="multipart/form-data">

                        <?= csrf_field(); ?>
                        <div class="form-group row">

                            <div class="col-sm-10">
                                <input type="hidden" class="form-control <?= ($validation->hasError('id_erita')) ? 'is-invalid' : ''; ?>" id="id_erita" name="id_erita" value="<?= old('id_erita'); ?>">
                                <div class="invalid-feedback">
                                    <?= $validation->getError('id_erita'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="text" class="col-sm-4 col-form-label">No. Kehilangan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control <?= ($validation->hasError('id_kehilangan')) ? 'is-invalid' : ''; ?>" id="id_kehilangan" name="id_kehilangan" autofocus value="<?= $no_kehilangan ?>" readonly>
                                <div class="invalid-feedback">
                                    <?= $validation->getError('id_kehilangan'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="penulis" class="col-sm-4 col-form-label">Nama Barang</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="nama_barang" name="nama_barang" value="<?= old('nama_barang'); ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="deskripsi" class="col-sm-4 col-form-label">Deskripsi</label>
                            <div class="col-sm-8">
                                <textarea type="text" class="form-control" rows="6" id="deskripsi" name="deskripsi" value="<?= old('deskripsi'); ?>"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="penulis" class="col-sm-4 col-form-label">No. Identitas</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="no_identitas" name="no_identitas" value="<?= old('no_identitas'); ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="penulis" class="col-sm-4 col-form-label">Alamat</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="alamat" name="alamat" value="<?= old('alamat'); ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="penulis" class="col-sm-4 col-form-label">No. HP</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="no_hp" name="no_hp" value="<?= old('no_hp'); ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="penulis" class="col-sm-4 col-form-label">Status temuan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="status_temuan" name="status_temuan" value="<?= old('status_temuan'); ?>">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary">Tambah</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main><!-- End #main -->
<?= $this->endSection(); ?>