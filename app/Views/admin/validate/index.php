<!-- memanggil template.php untuk mewariskan method -->

<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>



<main id="main" class="main">

    <div class="pagetitle">
        <h1>Validasi Booking</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Booking</a></li>
                <li class="breadcrumb-item active">Validate</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->


    <section class="section dashboard">
        <!-- News & Updates Traffic -->
        <div class="container">
            <div class="row">
                <div class="col">

                    <?php if (session()->getFlashdata('pesan')) : ?>
                        <div class="alert alert-success" role="alert">
                            <?= session()->getFlashdata('pesan'); ?>
                        </div>
                    <?php endif ?>
                </div>
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">No. Registrasi</th>
                            <th scope="col">Nama</th>
                            <th scope="col">No. Tlp</th>
                            <th scope="col">Bukti</th>
                            <th scope="col">Jumlah</th>
                            <th scope="col">Tanggal</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($bookingpembayaran as $k) : ?>
                            <tr>
                                <th scope="row"><?= $i++; ?></th>
                                <td><?= $k['no_registrasi']; ?></td>
                                <td><?= $k['nama']; ?></td>
                                <td><?= $k['no_hp']; ?></td>
                                <td>
                                    <a href="\img\bukti\<?= $k['bukti_pembayaran']; ?>" id="myImg" src="\img\bukti\<?= $k['bukti_pembayaran']; ?>" style="width:100%;max-width:300px" alt="Snow"><?= $k['bukti_pembayaran']; ?>
                                </td>
                                <td><?= $k['jumlah']; ?></td>
                                <td><?= $k['created_at']; ?></td>
                                <td><?= $k['status']; ?></td>
                                <td>

                                    <form action="/Validate/<?= $k['no_registrasi']; ?>" method="post" enctype="multipart/form-data">

                                        <?= csrf_field(); ?>
                                        <!-- <input type="hidden" name="_method" value="UPDATE"> -->
                                        <!-- <button type="submit" class="btn btn-primary" onclick="return confirm('Apakah anda yakin?');">Validasi</button> -->
                                        <button type="submit" class="btn btn-primary" data-id="<?= $k['no_hp']; ?>" onclick="return confirm('Apakah anda yakin?');" id="btn-validasi">Validasi</button>


                                    </form>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

            </div>
        </div><!-- End News & Updates -->


    </section>
</main><!-- End #main -->

<?= $this->endSection(); ?>

<script>

</script>