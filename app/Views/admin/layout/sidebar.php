  <!-- ======= Sidebar ======= -->
  <aside id="sidebar" class="sidebar">

      <ul class="sidebar-nav" id="sidebar-nav">

          <li class="nav-item">
              <a class="nav-link " href="/Admin/index">
                  <i class="bi bi-grid"></i>
                  <span>Dashboard</span>
              </a>
          </li><!-- End Dashboard Nav -->

          <li class="nav-item">
              <a class="nav-link collapsed" data-bs-target="#tables-nav" data-bs-toggle="collapse" href="#">
                  <i class="bi bi-box-arrow-in-right"></i><span>Data</span><i class="bi bi-chevron-down ms-auto"></i>
              </a>
              <ul id="tables-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                  <li>
                      <a href="tables-general.html">
                          <i class="bi bi-circle"></i><span>Petugas</span>
                      </a>
                  </li>

              </ul>
          </li><!-- End Tables Nav -->

          <li class="nav-item">
              <a class="nav-link collapsed" data-bs-target="#components-nav" data-bs-toggle="collapse" href="#">
                  <i class="bi bi-menu-button-wide"></i><span>Halaman</span><i class="bi bi-chevron-down ms-auto"></i>
              </a>
              <ul id="components-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                  <li>
                      <a href="/Berita">
                          <i class="bi bi-circle"></i><span>Berita</span>
                      </a>
                  </li>
                  <!-- <li>
                      <a href="components-breadcrumbs.html">
                          <i class="bi bi-circle"></i><span>Persyaratan</span>
                      </a>
                  </li>
                  <li>
                      <a href="/Sop">
                          <i class="bi bi-circle"></i><span>SOP</span>
                      </a>
                  </li> -->

              </ul>
          </li><!-- End Components Nav -->

          <li class="nav-item">
              <a class="nav-link collapsed" data-bs-target="#forms-nav" data-bs-toggle="collapse" href="/judulpages/booking">
                  <i class="bi bi-journal-text"></i><span>Booking</span><i class="bi bi-chevron-down ms-auto"></i>
              </a>
              <ul id="forms-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                  <li>
                      <a href="/Admin/Kuota">
                          <i class="bi bi-circle"></i><span>Kuota</span>
                      </a>
                  </li>
                  <li>
                      <a href="/Admin/Tarif">
                          <i class="bi bi-circle"></i><span>Tarif</span>
                      </a>
                  </li>
                  <li>
                      <a href="/Admin/Validate">
                          <i class="bi bi-circle"></i><span>Validasi</span>
                      </a>
                  </li>
                  <li>
                      <a href="/Admin/DataBooking">
                          <i class="bi bi-circle"></i><span>Data Booking</span>
                      </a>
                  </li>
              </ul>
          </li><!-- End Forms Nav -->



          <li class="nav-item">
              <a class="nav-link collapsed" data-bs-target="#charts-nav" data-bs-toggle="collapse" href="#">
                  <i class="bi bi-person"></i><span>Pendakian</span><i class="bi bi-chevron-down ms-auto"></i>
              </a>
              <ul id="charts-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                  <li>
                      <a href="/CrDataBooking/Berlangsung">
                          <i class="bi bi-circle"></i><span>Berlangsung</span>
                      </a>
                  </li>
                  <li>
                      <a href="/CrDataBooking/All">
                          <i class="bi bi-circle"></i><span>Data Pendakian</span>
                      </a>
                  </li>
              </ul>
          </li><!-- End Charts Nav -->

          <li class="nav-item">
              <a class="nav-link collapsed" data-bs-target="#icons-nav" data-bs-toggle="collapse" href="">
                  <i class="bi bi-gem"></i><span>Temuan</span><i class="bi bi-chevron-down ms-auto"></i>
              </a>
              <ul id="icons-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                  <li>
                      <a href="/Kehilangan">
                          <i class="bi bi-circle"></i><span>Kehilangan</span>
                      </a>
                  </li>
                  <li>
                      <a href="/Temuan">
                          <i class="bi bi-circle"></i><span>Ditemukan</span>
                      </a>
                  </li>
                  <li>
                      <a href="/AmbilTemuan">
                          <i class="bi bi-circle"></i><span>Diambil</span>
                      </a>
                  </li>
              </ul>
          </li><!-- End Icons Nav -->

          <li class="nav-heading">Pages</li>

          <li class="nav-item">
              <a class="nav-link collapsed" data-bs-target="#laporn" data-bs-toggle="collapse" href="#">
                  <i class="bi bi-card-list"></i><span>Laporan</span><i class="bi bi-chevron-down ms-auto"></i>
              </a>
              <ul id="laporn" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                  <li>
                      <a href="icons-bootstrap.html">
                          <i class="bi bi-circle"></i><span>Booking</span>
                      </a>
                  </li>
                  <li>
                      <a href="icons-remix.html">
                          <i class="bi bi-circle"></i><span>Pendakian</span>
                      </a>
                  </li>

              </ul>

  </aside><!-- End Sidebar-->