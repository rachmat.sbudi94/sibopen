<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title><?= $title; ?></title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo base_url('/imgadmin/favicon.png') ?>" rel="icon">
  <link href="<?php echo base_url('/imgadmin/apple-touch-icon.png') ?>" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url('/css/bootstrap.min.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('/vendor/bootstrap-icons/bootstrap-icons.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('/vendor/boxicons/css/boxicons.min.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('/vendor/quill/quill.snow.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('/vendor/quill/quill.bubble.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('/vendor/remixicon/remixicon.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('/vendor/simple-datatables/style.css') ?>" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo base_url('/css/styleadmin.css') ?>" rel="stylesheet">

  <!-- css scan web qr -->
  <link href="<?php echo base_url('/vendor/webcodecamjs/css/bootstrap.min.css') ?>" rel="stylesheet" />
  <link href="<?php echo base_url('/vendor/webcodecamjs/css/style.css') ?>" rel="stylesheet" />


  <!-- =======================================================
  * Template Name: NiceAdmin - v2.2.0
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->

</head>

<body>

  <?= $this->include('/admin/layout/header'); ?>
  <?= $this->include('/admin/layout/sidebar'); ?>

  <!-- mencetak section yang memanggil dari halaman (''); -->
  <?= $this->renderSection('content'); ?>


  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url('/vendor/apexcharts/apexcharts.min.js') ?>"></script>
  <script src="<?php echo base_url('/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
  <script src="<?php echo base_url('/vendor/chart.js/chart.min.js') ?>"></script>
  <script src="<?php echo base_url('/vendor/echarts/echarts.min.js') ?>"></script>
  <script src="<?php echo base_url('/vendor/quill/quill.min.js') ?>"></script>
  <script src="<?php echo base_url('/vendor/simple-datatables/simple-datatables.js') ?>"></script>
  <script src="<?php echo base_url('/vendor/tinymce/tinymce.min.js') ?>"></script>
  <script src="<?php echo base_url('/vendor/php-email-form/validate.js') ?>"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo base_url('/js/main.js') ?>"></script>
  <script src="<?php echo base_url('/vendor/ckeditor/ckeditor.js') ?>"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

  <!-- Merubah text input sesuai nama file yang di upload -->
  <script>
    function previewImg() {
      const gambar = document.querySelector('#gambar');
      const gambarLabel = document.querySelector('.custom-file-label');
      const imgPreview = document.querySelector('.img-preview');

      gambarLabel.textContent = gambar.files[0].name;

      const fileGambar = new FileReader();
      fileGambar.readAsDataURL(gambar.files[0]);

      fileGambar.onload = function(e) {
        imgPreview.src = e.target.result;
      }
    }
  </script>

  <!-- notif wa validasi -->
  <script>
    // // Replace the <textarea id="editor1"> with a CKEditor 4
    // // instance, using default configuration.
    // CKEDITOR.replace('deskripsi');
    $("#btn-validasi").on("click", function() {
      var number = $(this).data("id");
      console.log(number)
      $.ajax({
        url: "http://localhost:5010/send-message",
        type: "post",
        data: {
          message: 'Hallo, pesan ini dari Petugas Pendakian Penanggungan. Pembayaran anda sudah di validasi. Masukkan No.Registrasi ke pencarian website,silahkan download atau cetak etiket. Terimakasih',
          number: number,
        },
        dataType: 'json',
        success: function(response) {
          console.log(response);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
        }
      });
    })
  </script>

  <!-- notif wa kehilangan -->
  <script>
    // // Replace the <textarea id="editor1"> with a CKEditor 4
    // // instance, using default configuration.
    // CKEDITOR.replace('deskripsi');
    $("#btn-kehilangan").on("click", function() {
      var number = $("#editstatus").val();
      var nohilang = $("#no_kehilangan").val();
      var deskripsi = $("#deskripsi").val();
      var namabarang = $("#nama_barang").val();
      console.log(number)
      $.ajax({
        url: "http://localhost:5010/send-message",
        type: "post",
        data: {
          message: 'Hallo.. Ini pesan dari petugas Basecamp Penanggunggan. Laporan kehilangan : ' + nohilang + ' Nama Barang :' + namabarang + ', Deskripsi :' + deskripsi + '. #SudahDitemukan# . Silahkan diambil di BaseCamp Penanggungan.',
          number: number,
        },
        dataType: 'json',
        success: function(response) {
          console.log(response);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
        }
      });
    })
  </script>




  <!-- Using jquery version: -->
  <!--
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/qrcodelib.js"></script>
        <script type="text/javascript" src="js/webcodecamjquery.js"></script>
        <script type="text/javascript" src="js/mainjquery.js"></script>
    -->


</body>

</body>

</html>