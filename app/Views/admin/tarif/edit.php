<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Berita</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Halaman</li>
                <li class="breadcrumb-item">Tarif</li>
                <li class="breadcrumb-item active">Edit</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <h2 class="my-3">Form Edit Data Komik</h2>

                    <form action="/tarif/update/<?= $tarif['id_tarif']; ?>" method="post" enctype="multipart/form-data">

                        <?= csrf_field(); ?>

                        <div class="form-group row">

                            <div class="col-sm-10">
                                <input type="hidden" class="form-control <?= ($validation->hasError('id_erita')) ? 'is-invalid' : ''; ?>" id="id_erita" name="id_erita" autofocus value="<?= old('id_erita'); ?>">
                                <div class="invalid-feedback">
                                    <?= $validation->getError('id_erita'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="text" class="col-sm-2 col-form-label">No. tarif</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control <?= ($validation->hasError('id_tarif')) ? 'is-invalid' : ''; ?>" id="id_tarif" name="id_tarif" autofocus value="<?= (old('id_tarif')) ? old('id_tarif') : $tarif['id_tarif'] ?>">
                                <div class="invalid-feedback">
                                    <?= $validation->getError('id_tarif'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="penulis" class="col-sm-2 col-form-label">Kategori</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="kategori" name="kategori" value="<?= (old('kategori')) ? old('kategori') : $tarif['kategori'] ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="penulis" class="col-sm-2 col-form-label">Harga</label>
                            <div class="col-sm-10">
                                <input type="num" class="form-control" id="harga" name="harga" value="<?= (old('harga')) ? old('harga') : $tarif['harga'] ?>">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Edit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main><!-- End #main -->
<?= $this->endSection(); ?>