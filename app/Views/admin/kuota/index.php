<!-- memanggil template.php untuk mewariskan method -->

<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Kuota Pendakian</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Booking</a></li>
                <li class="breadcrumb-item active">Kuota</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->


    <section class="section dashboard">
        <!-- News & Updates Traffic -->
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="/Admin/Kuota/create" class="btn btn-primary mt-3">Tambah Kuota</a>
                    <?php if (session()->getFlashdata('pesan')) : ?>
                        <div class="alert alert-success" role="alert">
                            <?= session()->getFlashdata('pesan'); ?>
                        </div>
                    <?php endif ?>
                </div>
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">No. Kuota</th>
                            <th scope="col">Kategori</th>
                            <th scope="col">Nama Kuota</th>
                            <th scope="col">Jumlah</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($kuota as $k) : ?>
                            <tr>
                                <th scope="row"><?= $i++; ?></th>
                                <td><?= $k['id_kuota']; ?></td>
                                <td><?= $k['kategori']; ?></td>
                                <td><?= $k['nama_kuota']; ?></td>
                                <td><?= $k['jumlah_kuota']; ?></td>
                                <td>
                                    <a href="/Admin/Kuota/<?= $k['id_kuota']; ?>" class="btn btn-danger">Set</a>
                                    <a href="/Admin/Kuota/edit/<?= $k['id_kuota']; ?>" class="btn btn-success">Edit</a>

                                    <form action="/Admin/Kuota/delete/<?= $k['id_kuota']; ?>" method="post" class="d-inline">
                                        <?= csrf_field(); ?>
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-success" onclick="return confirm('Apakah anda yakin?');">Delete</button>
                                    </form>


                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

            </div>
        </div><!-- End News & Updates -->
    </section>
</main><!-- End #main -->
<?= $this->endSection(); ?>