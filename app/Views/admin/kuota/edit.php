<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Berita</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Halaman</li>
                <li class="breadcrumb-item">Kuota</li>
                <li class="breadcrumb-item active">Edit</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <h2 class="my-3">Form Edit Data Komik</h2>

                    <form action="/Admin/Kuota/update/<?= $kuota['id_kuota']; ?>" method="post" enctype="multipart/form-data">

                        <?= csrf_field(); ?>

                        <div class="form-group row">

                            <div class="col-sm-10">
                                <input type="hidden" class="form-control <?= ($validation->hasError('id_erita')) ? 'is-invalid' : ''; ?>" id="id_erita" name="id_erita" autofocus value="<?= old('id_erita'); ?>">
                                <div class="invalid-feedback">
                                    <?= $validation->getError('id_erita'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="text" class="col-sm-2 col-form-label">No. Kuota</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control <?= ($validation->hasError('id_kuota')) ? 'is-invalid' : ''; ?>" id="id_kuota" name="id_kuota" value="<?= (old('id_kuota')) ? old('id_kuota') : $kuota['id_kuota'] ?>" disabled>
                                <div class="invalid-feedback">
                                    <?= $validation->getError('id_kuota'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="penulis" class="col-sm-2 col-form-label">Kategori</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="kategori" name="kategori" value="<?= (old('kategori')) ? old('kategori') : $kuota['kategori'] ?>" autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="penulis" class="col-sm-2 col-form-label">Nama Kuota</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="nama_kuota" name="nama_kuota" value="<?= (old('nama_kuota')) ? old('nama_kuota') : $kuota['nama_kuota'] ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="penulis" class="col-sm-2 col-form-label">Jumlah Kuota</label>
                            <div class="col-sm-10">
                                <input type="num" class="form-control" id="jumlah_kuota" name="jumlah_kuota" value="<?= (old('jumlah_kuota')) ? old('jumlah_kuota') : $kuota['jumlah_kuota'] ?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Edit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main><!-- End #main -->
<?= $this->endSection(); ?>