<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Kuota</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Halaman</li>
                <li class="breadcrumb-item ">Kuota</li>
                <li class="breadcrumb-item active">Tambah Kuota</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <h2 class="my-3">Form Tambah Kuota</h2>

                    <form action="/Admin/Kuota/save" method="post" enctype="multipart/form-data">

                        <?= csrf_field(); ?>
                        <div class="form-group row">

                            <div class="col-sm-10">
                                <input type="hidden" class="form-control <?= ($validation->hasError('id_erita')) ? 'is-invalid' : ''; ?>" id="id_erita" name="id_erita" autofocus value="<?= old('id_erita'); ?>">
                                <div class="invalid-feedback">
                                    <?= $validation->getError('id_erita'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="text" class="col-sm-4 col-form-label">No. Kuota</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control <?= ($validation->hasError('id_kuota')) ? 'is-invalid' : ''; ?>" id="id_kuota" name="id_kuota" autofocus value="<?= old('id_kuota'); ?>">
                                <div class="invalid-feedback">
                                    <?= $validation->getError('id_kuota'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="penulis" class="col-sm-4 col-form-label">Kategori</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="kategori" name="kategori" value="<?= old('kategori'); ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="penulis" class="col-sm-4 col-form-label">Nama Kuota</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="nama_kuota" name="nama_kuota" value="<?= old('nama_kuota'); ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="penulis" class="col-sm-4 col-form-label">Jumlah Kuota</label>
                            <div class="col-sm-8">
                                <input type="num" class="form-control" id="jumlah_kuota" name="jumlah_kuota" value="<?= old('jumlah_kuota'); ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary">Tambah</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main><!-- End #main -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $("#kirim").click(function() {
        $.ajax({
            type: "POST",
            url: "https://pixodestyafandy-wa-api.herokuapp.com/chat/sendmessage/081331689223",
            dataType: "json",
            success: function(msg) {
                if (msg) {
                    alert("Somebody" + name + " was added in list !");
                    location.reload(true);
                } else {
                    alert("Cannot add to list !");
                }
            },
            data: {
                message: 'Halo cobababa'
            }
        });
    })
</script>
<?= $this->endSection(); ?>