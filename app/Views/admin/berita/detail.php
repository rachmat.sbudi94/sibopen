<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Berita</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Halaman</li>
                <li class="breadcrumb-item active">Berita</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h2>Detail Berita</h2>
                    <div class="card mb-3 my-2" style="max-width: 1500px;">
                        <div class="row g-0">
                            <div class="col-md-4 mt-5">
                                <img src="/imgadmin/imgberita/<?= $berita['gambar']; ?>" class="img-fluid rounded-start" alt="...">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title"><?= $berita['judul']; ?></h5>
                                    <p class="card-text"><b>Dibuat :</b><?= $berita['created_at']; ?></p>

                                    <p class="card-text"><small class="text-muted"><b>Deskripsi :</b><?= $berita['deskripsi']; ?></small></p>

                                    <a href="/Admin/Berita/edit/<?= $berita['id_berita']; ?>" class="btn btn-warning">Edit</a>
                                    <form action="/Admin/Berita/delete/<?= $berita['id_berita']; ?>" method="post" class="d-inline">
                                        <?= csrf_field(); ?>
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-danger" onclick="return confirm('Apakah anda yakin?');">Delete</button>
                                    </form>
                                    <br><br>
                                    <a href="/Admin/Berita/">Kembali</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main><!-- End #main -->
<?= $this->endSection(); ?>