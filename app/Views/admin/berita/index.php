<!-- memanggil template.php untuk mewariskan method -->

<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Berita Penanggungan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Halaman</li>
                <li class="breadcrumb-item active">Berita</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->


    <section class="section dashboard">
        <!-- News & Updates Traffic -->
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="/Admin/Berita/create" class="btn btn-primary mt-3">Tambah Berita</a>
                    <?php if (session()->getFlashdata('pesan')) : ?>
                        <div class="alert alert-success" role="alert">
                            <?= session()->getFlashdata('pesan'); ?>
                        </div>
                    <?php endif ?>

                    <div class="table-responsive">

                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">No. Berita</th>
                                    <th scope="col">Judul</th>
                                    <th scope="col">Kategori</th>
                                    <th scope="col">Gambar</th>
                                    <th scope="col">Dibuat</th>
                                    <th scope="col">Diperbarui</th>
                                    <th scope="col">Aksi</th>


                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                <?php foreach ($berita as $k) : ?>
                                    <tr>
                                        <th scope="row"><?= $i++; ?></th>
                                        <td><?= $k['id_berita']; ?></td>
                                        <td><?= $k['judul']; ?></td>
                                        <td><?= $k['kategori']; ?></td>
                                        <td><?= $k['gambar']; ?></td>
                                        <td><?= $k['created_at']; ?></td>
                                        <td><?= $k['updated_at']; ?></td>
                                        <td>
                                            <a href="/Admin/Berita/detail/<?= $k['id_berita']; ?>" class="btn btn-success">Detail</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>

                        </table>
                    </div>


                </div>
            </div>
        </div>
        <!-- End News & Updates -->
    </section>
</main><!-- End #main -->
<?= $this->endSection(); ?>