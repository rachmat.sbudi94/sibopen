<!-- memanggil template.php untuk mewariskan method -->

<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Dashboard</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->


    <section class="section dashboard">
        <!-- News & Updates Traffic -->
        <div class="container" id="QR-Code">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="navbar-form navbar-left">
                        <h4>Scan Tiket Pendakian</h4>
                    </div>
                    <div class="navbar-form navbar-right">
                        <select class="form-control" id="camera-select"></select>
                        <div class="form-group">
                            <input id="image-url" type="text" class="form-control" placeholder="Image url" />
                            <button title="Decode Image" class="btn btn-default btn-sm" id="decode-img" type="button" data-toggle="tooltip">
                                <span class="glyphicon glyphicon-upload"></span>
                            </button>
                            <button title="Image shoot" class="btn btn-info btn-sm disabled" id="grab-img" type="button" data-toggle="tooltip">
                                <span class="glyphicon glyphicon-picture"></span>
                            </button>
                            <button title="Play" class="btn btn-success btn-sm" id="play" type="button" data-toggle="tooltip">
                                <span class="glyphicon glyphicon-play"></span>
                            </button>
                            <button title="Pause" class="btn btn-warning btn-sm" id="pause" type="button" data-toggle="tooltip">
                                <span class="glyphicon glyphicon-pause"></span>
                            </button>
                            <button title="Stop streams" class="btn btn-danger btn-sm" id="stop" type="button" data-toggle="tooltip">
                                <span class="glyphicon glyphicon-stop"></span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="panel-body text-center">
                    <div class="col-md-6">
                        <div class="well" style="position: relative; display: inline-block">
                            <canvas width="320" height="240" id="webcodecam-canvas"></canvas>
                            <div class="scanner-laser laser-rightBottom" style="opacity: 0.3"></div>
                            <div class="scanner-laser laser-rightTop" style="opacity: 0.3"></div>
                            <div class="scanner-laser laser-leftBottom" style="opacity: 0.3"></div>
                            <div class="scanner-laser laser-leftTop" style="opacity: 0.3"></div>
                        </div>
                        <div class="well" style="width: 40%">
                            <label id="zoom-value" width="40">Zoom: 2</label>
                            <input id="zoom" onchange="Page.changeZoom();" type="range" min="10" max="30" value="20" />
                            <label id="brightness-value" width="40">Brightness: 0</label>
                            <input id="brightness" onchange="Page.changeBrightness();" type="range" min="0" max="128" value="0" />
                            <label id="contrast-value" width="40">Contrast: 0</label>
                            <input id="contrast" onchange="Page.changeContrast();" type="range" min="0" max="64" value="0" />
                            <label id="threshold-value" width="40">Threshold: 0</label>
                            <input id="threshold" onchange="Page.changeThreshold();" type="range" min="0" max="512" value="0" />
                            <label id="sharpness-value" width="40">Sharpness: off</label>
                            <input id="sharpness" onchange="Page.changeSharpness();" type="checkbox" />
                            <label id="grayscale-value" width="40">grayscale: off</label>
                            <input id="grayscale" onchange="Page.changeGrayscale();" type="checkbox" />
                            <br />
                            <label id="flipVertical-value" width="40">Flip Vertical: off</label>
                            <input id="flipVertical" onchange="Page.changeVertical();" type="checkbox" />
                            <label id="flipHorizontal-value" width="40">Flip Horizontal: off</label>
                            <input id="flipHorizontal" onchange="Page.changeHorizontal();" type="checkbox" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="thumbnail" id="result">
                            <div class="well" style="overflow: hidden">
                                <img width="320" height="240" id="scanned-img" src="" />
                            </div>
                            <div class="caption">
                                <h3>Scanned result</h3>

                                <form action="/Admin/CrDataBooking/index" method="post">
                                    <div class="d-flex justify-content-end bg-success">
                                        <!-- <p name="keyword" id="scanned-QR"></p> -->
                                        <input id="scanned-QR" type="text" class="form-control me-2" name="keyword">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-light" type="submit" name="submit">Cari</button>
                                        </div>
                                    </div>
                                </form>
                                <div class="form-group row">
                                    <label for="text" class="col-sm-4 col-form-label">No. Kehilangan</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control <?= ($validation->hasError('id_kehilangan')) ? 'is-invalid' : ''; ?>" id="id_kehilangan" name="id_kehilangan" autofocus value="<?= $no_kehilangan ?>" readonly>
                                        <div class="invalid-feedback">
                                            <?= $validation->getError('id_kehilangan'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main><!-- End #main -->

<?= $this->endSection(); ?>