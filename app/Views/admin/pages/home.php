<!-- memanggil template.php untuk mewariskan method -->

<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Dashboard</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->


    <section class="section dashboard">
        <!-- News & Updates Traffic -->
        <div class="container">
            <div class="row">
                <div class="col-sm-6"> </div>
                <div class="col-sm-6 text-start">
                    <h4 class="text-warning fw-bold"> <i class="bi bi-exclamation-triangle-fill"></i> Pembayaran: <?= empty($status['status']) ? '' : $status['status']; ?></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row justify-content-around">
                        <div class="card justify-content-center col-md-5 ">
                            <div class="card-body">
                                <video id="previewKamera" style="width: 300px;height: 300px;"></video>
                                <select class="form-select" id="pilihKamera" style="max-width:500px">
                                </select>
                                <form action="/Admin/CrDataBooking/index" method="post">
                                    <div class="d-flex justify-content-end bg-success">
                                        <!-- <p name="keyword" id="scanned-QR"></p> -->
                                        <input id="hasilscan" type="text" class="form-control me-2" name="keyword">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-light" type="submit" name="submit">Cari</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <div class="card justify-content-center col-md-7 ">
                            <div class="card-body">
                                <h5 class="card-header">Data Tiket Pendakian</h5>
                                <?php if (session()->getFlashdata('pesan1')) : ?>
                                    <div class="alert alert-success" role="alert">
                                        <?= session()->getFlashdata('pesan1'); ?>
                                    </div>
                                <?php endif ?>
                                <form action="/CrDataBooking/save" method="post" enctype="multipart/form-data">
                                    <div class="col-md-12 ">
                                        <div class="form-group row">
                                            <label for="no_registrasi" class="col-sm-4 col-form-label">No. Registrasi</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="no_registrasi" name="no_registrasi" value="<?= empty($ketua['no_registrasi']) ? '' : $ketua['no_registrasi'] ?> " readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="nama" class="col-sm-4 col-form-label">Nama</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="nama" name="nama" value="<?= empty($ketua['nama']) ? '' : $ketua['nama'] ?> " readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="no_identitas" class="col-sm-4 col-form-label">No. Identitas</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="no_identitas" name="no_identitas" value="<?= empty($ketua['no_identitas']) ? '' : $ketua['no_identitas'] ?> " readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="jalur" class="col-sm-4 col-form-label">Jalur</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="jalur" name="jalur" value="<?= empty($ketua['jalur']) ? '' : $ketua['jalur'] ?> " readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="tgl_pendakian" class="col-sm-4 col-form-label">Request Tanggal</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="tgl_pendakian" name="tgl_pendakian" value="<?= empty($ketua['tgl_pendakian']) ? '' : $ketua['tgl_pendakian'] ?> " readonly>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Nama Anggota</th>
                                                        <th scope="col">No. Identitas</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = 1;

                                                    foreach ($anggota as $row) : ?>
                                                        <tr>
                                                            <td><?= $no++; ?></td>
                                                            <td><?= empty($row['nama_anggota']) ? '' : $row['nama_anggota']; ?></td>
                                                            <td><?= empty($row['no_identitasanggota']) ? '' : $row['no_identitasanggota']; ?></td>

                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                            <div class="col-12 text-end">
                                                <button type="submit" class="btn btn-primary btn">Simpan</button>
                                            </div>
                                        </div>
                                    </div>
                            </div>


                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- <input type="text" id="hasilscan"> -->

        <script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

        <script>
            let selectedDeviceId = null;
            const codeReader = new ZXing.BrowserMultiFormatReader();
            const sourceSelect = $("#pilihKamera");

            $(document).on('change', '#pilihKamera', function() {
                selectedDeviceId = $(this).val();
                if (codeReader) {
                    codeReader.reset()
                    initScanner()
                }
            })

            function initScanner() {
                codeReader
                    .listVideoInputDevices()
                    .then(videoInputDevices => {
                        videoInputDevices.forEach(device =>
                            console.log(`${device.label}, ${device.deviceId}`)
                        );

                        if (videoInputDevices.length > 0) {

                            if (selectedDeviceId == null) {
                                if (videoInputDevices.length > 1) {
                                    selectedDeviceId = videoInputDevices[1].deviceId
                                } else {
                                    selectedDeviceId = videoInputDevices[0].deviceId
                                }
                            }


                            if (videoInputDevices.length >= 1) {
                                sourceSelect.html('');
                                videoInputDevices.forEach((element) => {
                                    const sourceOption = document.createElement('option')
                                    sourceOption.text = element.label
                                    sourceOption.value = element.deviceId
                                    if (element.deviceId == selectedDeviceId) {
                                        sourceOption.selected = 'selected';
                                    }
                                    sourceSelect.append(sourceOption)
                                })

                            }

                            codeReader
                                .decodeOnceFromVideoDevice(selectedDeviceId, 'previewKamera')
                                .then(result => {

                                    //hasil scan
                                    console.log(result.text)
                                    $("#hasilscan").val(result.text);

                                    if (codeReader) {
                                        codeReader.reset()
                                    }
                                })
                                .catch(err => console.error(err));

                        } else {
                            alert("Camera not found!")
                        }
                    })
                    .catch(err => console.error(err));
            }


            if (navigator.mediaDevices) {


                initScanner()


            } else {
                alert('Cannot access camera.');
            }
        </script>
        </div>
    </section>

</main><!-- End #main -->

<?= $this->endSection(); ?>