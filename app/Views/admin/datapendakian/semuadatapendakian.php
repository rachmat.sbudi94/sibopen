<!-- memanggil template.php untuk mewariskan method -->

<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Data Pendakian</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Pendakian</a></li>
                <li class="breadcrumb-item active">Data Pendakian</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->


    <section class="section dashboard">
        <!-- News & Updates Traffic -->
        <div class="container">
            <a target="_blank" href="/export-pdf" class="btn btn-warning">PrintPdf</a>
            <div class="row">
                <div class="col">

                    <?php if (session()->getFlashdata('pesan')) : ?>
                        <div class="alert alert-success" role="alert">
                            <?= session()->getFlashdata('pesan'); ?>
                        </div>
                    <?php endif ?>
                </div>
                <div class="table-responsive">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">No. Registrasi</th>
                                <th scope="col">Cek In</th>
                                <th scope="col">Ketua</th>
                                <th scope="col">No Identitas</th>
                                <th scope="col">Kwarganegaraan</th>
                                <th scope="col">Alamat</th>
                                <th scope="col">Cek Out</th>
                                <th scope="col">Tgl. CekOut</th>
                                <th scope="col">Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($datapendakian as $k) : ?>
                                <tr>
                                    <th scope="row"><?= $i++; ?></th>
                                    <td><?= $k['no_registrasi']; ?></td>
                                    <td><?= $k['tgl_cekin']; ?></td>
                                    <td><?= $k['nama']; ?></td>
                                    <td><?= $k['no_identitas']; ?></td>
                                    <td><?= $k['kwarganegaraan']; ?></td>
                                    <td><?= $k['alamat']; ?></td>
                                    <td><?= $k['cekout']; ?></td>
                                    <td><?= $k['tgl_cekout']; ?></td>
                                    <td>
                                        <a href="/DetailPendakian/<?= $k['no_registrasi']; ?>" class="btn btn-success btn-sm"><i class="bi bi-people-fill"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- End News & Updates -->
    </section>
</main><!-- End #main -->
<?= $this->endSection(); ?>