<!-- memanggil template.php untuk mewariskan method -->

<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Data Pendakian</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Pendakian</a></li>
                <li class="breadcrumb-item active">Data Pendakian</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->


    <section class="section dashboard">
        <!-- News & Updates Traffic -->
        <div class="container">

            <div class="row">
                <div class="col">
                    <h3>No. Registrasi : <?= $ketua['no_registrasi']; ?></h3>
                    <h3>Nama Ketua : <?= $ketua['nama']; ?> </h3>
                </div>
            </div>
            <div class="row">
                <div class="col">
                </div>
                <div class="table-responsive">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama Anggota</th>
                                <th scope="col">No.Tlp</th>
                                <th scope="col">No Identitas</th>
                                <th scope="col">Kwarganegaraan</th>
                                <th scope="col">Jenkel</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($anggota as $k) : ?>
                                <tr>
                                    <th scope="row"><?= $i++; ?></th>
                                    <td><?= $k['nama_anggota']; ?></td>
                                    <td><?= $k['no_tlpanggota']; ?></td>
                                    <td><?= $k['no_identitasanggota']; ?></td>
                                    <td><?= $k['kwarganegaraan_anggota']; ?></td>
                                    <td><?= $k['jenis_kelaminanggota']; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- End News & Updates -->
    </section>
</main><!-- End #main -->
<?= $this->endSection(); ?>