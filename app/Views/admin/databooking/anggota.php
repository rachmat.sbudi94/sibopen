<!-- memanggil template.php untuk mewariskan method -->

<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Data Pendakian</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Pendakian</a></li>
                <li class="breadcrumb-item active">Data Pendakian</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->


    <section class="section dashboard">
        <!-- News & Updates Traffic -->
        <div class="container">
            <div class="row ">
                <div class="col-md-5 d-flex align-items-stretch ">
                    <div class="card ">
                        <div class="card-body mt-4 ">
                            <h4 class="">No. Registrasi : <strong><?= $ketua['no_registrasi']; ?></strong></h4>
                            <h4>Nama Ketua : <?= $ketua['nama']; ?> </h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="card">
                        <div class="row">
                            <h4 class="text-center mt-2"><strong><ins> Kontak Darurat </ins></strong></h4>
                            <div class="col-6">
                                <div class="card-body">

                                    <h5>Nama : <?= $kontakdarurat['nama_darurat1']; ?> </h5>
                                    <h5>Hubungan : <?= $kontakdarurat['hubungan_darurat1']; ?> </h5>
                                    <h5>Telp : <?= $kontakdarurat['tlp_darurat1']; ?> </h5>

                                </div>

                            </div>
                            <div class="col-6">

                                <div class="card-body">

                                    <h5>Nama : <?= $kontakdarurat['nama_darurat2']; ?> </h5>
                                    <h5>Hubungan : <?= $kontakdarurat['hubungan_darurat2']; ?> </h5>
                                    <h5>Telp : <?= $kontakdarurat['tlp_darurat2']; ?> </h5>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nama Anggota</th>
                                        <th scope="col">No.Tlp</th>
                                        <th scope="col">No Identitas</th>
                                        <th scope="col">Kwarganegaraan</th>
                                        <th scope="col">Jenkel</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($anggota as $k) : ?>
                                        <tr>
                                            <th scope="row"><?= $i++; ?></th>
                                            <td><?= $k['nama_anggota']; ?></td>
                                            <td><?= $k['no_tlpanggota']; ?></td>
                                            <td><?= $k['no_identitasanggota']; ?></td>
                                            <td><?= $k['kwarganegaraan_anggota']; ?></td>
                                            <td><?= $k['jenis_kelaminanggota']; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


        </div><!-- End News & Updates -->
    </section>
</main><!-- End #main -->
<?= $this->endSection(); ?>