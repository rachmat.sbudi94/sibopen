<!-- memanggil template.php untuk mewariskan method -->

<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Data Booking</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Booking</a></li>
                <li class="breadcrumb-item active">Data Booking</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->


    <section class="section dashboard">
        <!-- News & Updates Traffic -->
        <div class="container">

            <div class="row">
                <div class="col-md-9 d-flex justify-content-start">
                    <form action="/PeriodeBooking" id="form1" method="post">
                        <div class="form-group">
                            <label class="control-label col-md-2 mt-2" for="">Tanggal</label>
                            <input type="date" id="tgl_awal" name="tgl_awal" style="width:auto;" value="<?= empty($tgl_awal) ? '' : $tgl_awal ?>">
                            <label>s/d</label>
                            <input type="date" name="tgl_akhir" style="width:auto;" value="<?= empty($tgl_akhir) ? '' : $tgl_akhir ?>">
                            <button type="submit" class="btn btn-success btn-sm">Tampilkan</button>
                            <a class="btn btn-secondary btn-sm" href="/Admin/DataBooking">Reset</a>
                        </div>
                    </form>
                </div>
                <div class="col-md-3 d-flex justify-content-end">
                    <form action="/exportperiodebooking-pdf" id="form1" method="post">
                        <div class="form-group">
                            <label class="control-label" for=""></label>
                            <input type="hidden" id="tgl_awal" name="tgl_awal" style="width:auto;" value="<?= empty($tgl_awal) ? '' : $tgl_awal ?>">
                            <input type="hidden" id="tgl_akhir" name="tgl_akhir" style="width:auto;" value="<?= empty($tgl_akhir) ? '' : $tgl_akhir ?>">
                            <button type="submit" class="btn btn-primary">Print Periode</button>
                            <a target="_blank" href="/export-pdf" class="btn btn-warning">Print All</a>
                        </div>
                    </form>

                </div>
            </div>
            <div class="row">
                <div class="col">

                    <?php if (session()->getFlashdata('pesan')) : ?>
                        <div class="alert alert-success" role="alert">
                            <?= session()->getFlashdata('pesan'); ?>
                        </div>
                    <?php endif ?>
                </div>
                <div class="table-responsive">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">No. Registrasi</th>
                                <th scope="col">Tgl Pendakian</th>
                                <th scope="col">Ketua</th>
                                <th scope="col">No Identitas</th>
                                <th scope="col">Kwarganegaraan</th>
                                <th scope="col">Tgl Lahir</th>
                                <th scope="col">Alamat</th>
                                <th scope="col">Detail</th>


                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($databooking as $k) : ?>
                                <tr>
                                    <th scope="row"><?= $i++; ?></th>
                                    <td><?= $k['no_registrasi']; ?></td>
                                    <td><?= $k['tgl_pendakian']; ?></td>
                                    <td><?= $k['nama']; ?></td>
                                    <td><?= $k['no_identitas']; ?></td>
                                    <td><?= $k['kwarganegaraan']; ?></td>
                                    <td><?= $k['tgl_lahir']; ?></td>
                                    <td><?= $k['alamat']; ?></td>
                                    <td>
                                        <a href="/DetailBooking/<?= $k['no_registrasi']; ?>" class="btn btn-success btn-sm"><i class="bi bi-people-fill"></i></a>
                                    </td>

                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- End News & Updates -->
    </section>
</main><!-- End #main -->
<?= $this->endSection(); ?>