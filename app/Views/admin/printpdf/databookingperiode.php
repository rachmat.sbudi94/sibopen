<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .border-table {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            width: 100%;
            border-collapse: collapse;
            text-align: center;
            font-size: 12px;
        }

        .border-table th {
            border: 1 solid #000;
            font-weight: bold;
            background-color: #e1e1e1;
        }

        .border-table td {
            border: 1 solid #000;
        }
    </style>
</head>

<body>
    <center>
        <h3>Laporan Booking</h3>
        <p>Periode <?= $tgl_awal ?> sampai <?= $tgl_akhir ?></p>
    </center>
    <table class="border-table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">No. Registrasi</th>
                <th scope="col">Tgl Pendakian</th>
                <th scope="col">Ketua</th>
                <th scope="col">No Identitas</th>
                <th scope="col">Kwarganegaraan</th>
                <th scope="col">Tgl Lahir</th>
                <th scope="col">Alamat</th>
                <th scope="col">Anggota</th>
                <th scope="col">No. Identitas</th>


            </tr>
        </thead>
        <tbody>
            <?php $i = 1; ?>
            <?php foreach ($databooking as $k) : ?>
                <tr>
                    <th scope="row"><?= $i++; ?></th>
                    <td><?= $k['no_registrasi']; ?></td>
                    <td><?= $k['tgl_pendakian']; ?></td>
                    <td><?= $k['nama']; ?></td>
                    <td><?= $k['no_identitas']; ?></td>
                    <td><?= $k['kwarganegaraan']; ?></td>
                    <td><?= $k['tgl_lahir']; ?></td>
                    <td><?= $k['alamat']; ?></td>
                    <td><?= $k['nama_anggota']; ?></td>
                    <td><?= $k['no_identitasanggota']; ?></td>



                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>

</html>