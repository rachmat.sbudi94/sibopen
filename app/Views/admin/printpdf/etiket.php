<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Tiket Booking Penanggungan</title>
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <!--<link rel="stylesheet" href="css/style.css" type="text/css">-->
</head>
<?php
include "vendor/qr_code/qrlib.php";    // Ini adalah letak pemyimpanan plugin qrcode

$tempdir = "img/imgqrcode/";        // Nama folder untuk pemyimpanan file qrcode

if (!file_exists($tempdir))        //jika folder belum ada, maka buat
    mkdir($tempdir);

// berikut adalah parameter qr code
$teks_qrcode    = $pembayaran['no_registrasi'];
$namafile        = $pembayaran['no_registrasi'] . ".png";
$quality        = "H"; // ini ada 4 pilihan yaitu L (Low), M(Medium), Q(Good), H(High)
$ukuran            = 5; // 1 adalah yang terkecil, 10 paling besar
$padding        = 1;

QRCode::png($teks_qrcode, $tempdir . $namafile, $quality, $ukuran, $padding);
?>

<body style="background-color: whitesmoke">
    <div class="kolom" style="background-color:white; width:700px; height:660px; margin:auto; margin-top:20px; margin-bottom: 20px; box-shadow: 3px 3px 5px 6px #ccc; ">
        <div class="col-md-6">
            <h3 style="font-family:Arial; margin-left: 34px;">E-tiket</h3>
            <p style="margin-left: 34px;">Detail Pendakian</p>
        </div>
        <!-- <div class="col-md-6">
            <img src="http://placehold.it/50x50">
        </div> -->

        <div class="col-md-8" style="background-color: white; margin-top:0px; border: 3px solid gainsboro; width: 600px; height: 550px; margin-bottom:0px; margin-left: 49px;">
            <div class="col-md-6">
                <img style="margin-left: 510px;" src="img/lmdh.jpg" width="80" height="80" class="media-object" alt="QR Code">
                <p style="margin: 0px;">Kode Booking Pendakian</p>
                <p style="font-size: 20px; font-weight:bold; margin: 0px; "><?= $pembayaran['no_registrasi']; ?> (<?= $pembayaran['tgl_pendakian']; ?>)
                    <hr style="width: 520px; margin:0px; color:#000; border: solid 1px black;">
                </p>
                <p style="margin-left: 6px; font-size: 14px;">Nama : <?= $pembayaran['nama']; ?></p>
                <p style="margin-left: 6px;  font-size: 14px;;">No. Identitas : <?= $pembayaran['no_identitas']; ?></p>
                <p style=" margin-left: 6px; font-size: 14px;">Alamat : <?= $pembayaran['alamat']; ?></p>

            </div>
            <div class="col-md-6">
                <p style="margin-left: 2px;">Anggota Pendakian</p>
                <table class="table" style=" width: 600px; margin:0px; border: 2px solid grey; ">

                    <thead style="background-color: grey;">
                        <tr style="font-size: 14px;">
                            <th scope="col">#</th>
                            <th scope="col">Nama Anggota</th>
                            <th scope="col">No. Identitas</th>
                            <th scope="col">Jenis Kelamin</th>
                        </tr>
                    </thead>
                    <tbody style=" border-top: 2px solid grey;">
                        <?php $no = 1 + (10 * ($page - 1));
                        foreach ($anggota as $row) : ?>
                            <tr style="font-size: 14px;">
                                <td><?= $no++; ?></td>
                                <td><?= $row['nama_anggota']; ?></td>
                                <td><?= $row['no_identitasanggota']; ?></td>
                                <td><?= $row['jenis_kelaminanggota']; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

            </div>
            <div class="row">
                <div class="col-md-8" style="margin-left: -40px;">
                    <ul style="padding-bottom:0px;"><strong>NOTE:</strong>
                        <li style="margin-left: 18px; padding-bottom: 2px; font-size: 10px;">Tunjukkan E-Tiket ke petugas BaseCamp</li>
                        <li style="margin-left: 18px; font-size: 10px;">Mohon taati <strong>SOP Pendakian</strong> . Terimakasih</li>

                    </ul>
                </div>

                <div class="col-md-4" style="margin-top: 10px; margin-left: 420px; width: 100px;">
                    <!--<div class="media">-->
                    <!--<div class="media-left">-->
                    <img src="img/imgqrcode/<?= $pembayaran['no_registrasi']; ?>.png" width="80" height="80" class="media-object" alt="QR Code">
                </div>
            </div>


        </div>

    </div>
    </div>


</body>

</html>