<!-- memanggil template.php untuk mewariskan method -->

<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Ambil Temuan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Temuan</a></li>
                <li class="breadcrumb-item active">Diambil</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->


    <section class="section dashboard">
        <!-- News & Updates Traffic -->
        <div class="container">

            <div class="row">
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">No. Diambil</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Deskripsi</th>
                            <th scope="col">No. Identitas</th>
                            <th scope="col">Alamat</th>
                            <th scope="col">No. HP</th>
                            <th scope="col">Keterangan</th>
                            <th scope="col">Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($kehilangan as $k) : ?>
                            <tr>
                                <th scope="row"><?= $i++; ?></th>
                                <td><?= $k['id_ambiltemuan']; ?></td>
                                <td><?= $k['nama_barang']; ?></td>
                                <td><?= $k['deskripsi']; ?></td>
                                <td><?= $k['no_identitas']; ?></td>
                                <td><?= $k['alamat']; ?></td>
                                <td><?= $k['no_hp']; ?></td>
                                <td><?= $k['keterangan']; ?></td>
                                <td><?= $k['created_at']; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

            </div>
        </div><!-- End News & Updates -->
    </section>
</main><!-- End #main -->
<?= $this->endSection(); ?>