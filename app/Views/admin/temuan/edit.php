<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Temuan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">temuan</li>
                <li class="breadcrumb-item">Temuan</li>
                <li class="breadcrumb-item active">Edit</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <h2 class="my-3">Form Edit Temuan</h2>

                    <form action="/Temuan/update/<?= $temuan['id_temuan']; ?>" method="post" enctype="multipart/form-data">

                        <?= csrf_field(); ?>

                        <div class="form-group row">

                            <div class="col-sm-10">
                                <input type="hidden" class="form-control <?= ($validation->hasError('id_erita')) ? 'is-invalid' : ''; ?>" id="id_erita" name="id_erita" autofocus value="<?= old('id_erita'); ?>">
                                <div class="invalid-feedback">
                                    <?= $validation->getError('id_erita'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="text" class="col-sm-2 col-form-label">No. temuan</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control <?= ($validation->hasError('id_temuan')) ? 'is-invalid' : ''; ?>" id="id_temuan" name="id_temuan" autofocus value="<?= (old('id_temuan')) ? old('id_temuan') : $temuan['id_temuan'] ?>">
                                <div class="invalid-feedback">
                                    <?= $validation->getError('id_temuan'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nama_barang" class="col-sm-2 col-form-label">Nama Barang</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="nama_barang" name="nama_barang" value="<?= (old('nama_barang')) ? old('nama_barang') : $temuan['nama_barang'] ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" id="deskripsi" name="deskripsi"><?= (old('deskripsi')) ? old('deskripsi') : $temuan['deskripsi'] ?></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="keterangan" class="col-sm-2 col-form-label">Keterangan</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="keterangan" name="keterangan" value="<?= (old('keterangan')) ? old('keterangan') : $temuan['keterangan'] ?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="status_temuan" class="col-sm-2 col-form-label">Status Temuan</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="status_temuan" name="status_temuan" value="<?= (old('status_temuan')) ? old('status_temuan') : $temuan['status_temuan'] ?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Edit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main><!-- End #main -->
<?= $this->endSection(); ?>