<!-- memanggil template.php untuk mewariskan method -->

<?= $this->extend('/admin/layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Temuan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Booking</a></li>
                <li class="breadcrumb-item active">temuan</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->


    <section class="section dashboard">
        <!-- News & Updates Traffic -->
        <div class="container">

            <div class="row">
                <div class="col">
                    <a href="/Admin/Temuan/create" class="btn btn-primary mt-3">Tambah temuan</a>
                    <?php if (session()->getFlashdata('pesan')) : ?>
                        <div class="alert alert-success" role="alert">
                            <?= session()->getFlashdata('pesan'); ?>
                        </div>
                    <?php endif ?>
                </div>
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">No. Temuan</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Deskripsi</th>

                            <th scope="col">Keterangan</th>
                            <th scope="col">Status Temuan</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($temuan as $k) : ?>
                            <tr>
                                <th scope="row"><?= $i++; ?></th>
                                <td><?= $k['id_temuan']; ?></td>
                                <td><?= $k['nama_barang']; ?></td>
                                <td><?= $k['deskripsi']; ?></td>
                                <td><?= $k['keterangan']; ?></td>
                                <td><?= $k['status_temuan']; ?></td>

                                <td>
                                    <a href="/Admin/Temuan/edit/<?= $k['id_temuan']; ?>" class="btn btn-primary btn-sm">Detail</a>
                                    <a href="/Admin/AmbilTemuanDitemukan/create/<?= $k['id_temuan']; ?>" class="btn btn-success btn-sm">Diambil</a>

                                    <form action="/Admin/Temuan/delete/<?= $k['id_temuan']; ?>" method="post" class="d-inline">
                                        <?= csrf_field(); ?>
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah anda yakin?');">Delete</button>
                                    </form>


                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

            </div>
        </div><!-- End News & Updates -->
    </section>
</main><!-- End #main -->
<?= $this->endSection(); ?>