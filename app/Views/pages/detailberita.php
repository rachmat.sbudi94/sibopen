<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>


<div class="container">
    <div class="row">
        <div class="col">
            <h3 class="text-center mt-3">Detail Berita</h3>
            <div class="card mb-3 my-1" style="max-width: 1500px;">
                <div class="row g-0">
                    <div class="col-md-4 mt-2">
                        <img src="/imgadmin/imgberita/<?= $berita['gambar']; ?>" class="img-fluid rounded-start" alt="...">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= $berita['judul']; ?></h5>
                            <p class="card-text"><b>Dibuat :</b><?= $berita['created_at']; ?></p>

                            <p class="card-text"><small class="text-muted"><b>Deskripsi :</b><?= $berita['deskripsi']; ?></small></p>
                            <br>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="container-fluid text-end mb-5 mt-1">
        <a href="/pages/home.php#beritainfo" class="btn btn-primary btn-sm">Kembali</a>
    </div>
    <?= $this->endSection(); ?>