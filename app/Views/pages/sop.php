<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>


<div class="container">
    <div class="row">
        <div class="col">
            <h1 class="text-center mt-3">SOP Pendakian Penanggungan</h1>
            <div class="card">
                <div class="card-body">
                    <h6 class="fw-bold">Latar Belakang</h6>
                    <p>Meningkatkan keselamatan pengunjung dan menjaga kelestarian ekosistem maka diperlukan pengaturan pendakian. Dengan adanya tanpengaturan pendakian diharapkan pengelolaan aktifitas pendakian dapat berjalan lebih efektif dan optimal sebagai wujud pelayanan prima pendakian.</p>
                    <hr />
                    <h6 class="fw-bold">Maksud dan Tujuan</h6>
                    <p>Maksud penyusunan Petunjuk Teknis Standar Operasional Prosedur Pendakian Gunung Penanggungan ini adalah meningkatnya keselamatan pendaki dan menjaga kelestarian ekosistem Gunung Penanggungan. Tujuan Tersedianya Standar Operasional Prosedur Pendakian sebagai pedoman pelaksanaan penyelenggaraan pendakian Gunung Penanggungan.</p>
                    <hr />
                    <h6 class="fw-bold">Ruang Lingkup</h6>
                    <p>Ruang lingkup Petunjuk Teknis SOP Pendakian Gunung Penanggungan ini meliputi landasan hukum arahan teknis prosedur pendakian pelaksanaan pendakian tugas dan tanggung jawab petugas pelayanan pendakian.</p>
                    <hr />
                    <h6 class="fw-bold">Teknis Pendakian</h6>
                    <p>Teknis pendakian dibagi ke dalam dua bagian, yaitu :</p>
                    <p>1. Persiapan fisik : Pendakian gunung termasuk ke dalam salah satu olaharaga berat yang menuntut fisik yang prima. Untuk mendukung hal tersebut, maka diperlukan persiapan fisik yang memadai. Persiapan fisik yang baik akan menunjang kelancaran kegiatan pendakian dan menghindarkan anggota pendakian dari cedera fisik. Persiapan fisik tersebut dapat berupa jogging, push-up dan vertical running. Persiapan fisik ini dapat disesuaikan dengan kebutuhan masing-masing individu dan medan yang akan ditempuh.</p>
                    <p>2. Persiapan mental : Mental adalah kondisi psikologis dari diri seseorang. Persiapan mental yang buruk sebelum melakukan kegiatan gunung-hutan akan mengakibatkan terganggunya kelancaran kegiatan tersebut.</p>
                    <p>3. Persiapan peralatan dan logistik : Peralatan dan logistik yang akan dibawa dalam pendakian disesuaikan dengan tujuan dari pendakian tersebut, medan yang akan dihadapi, dan lamanya waktu pendakian.Setiap peserta kegiatan pendakian diharuskan untuk mengisi checklist peralatan pribadi, sedangkan pemimpin kegiatan pendakian diharuskan untuk mengumpulkan serta menyimpan checklist perlengkapan kelompok dan checklist perlengkapan pribadi seluruh peserta kegiatan pendakian. Checklist peralatan ini akan menjadi kartu kontrol yang dapat digunakan oleh pemimpin kegiatan pendakian untuk mengecek kelengkapan peralatan, mengevaluasi kesiapan anggota tim untuk melakukan survival dalam keadaan terburuk, dan memperkirakan batas waktu anggota tim untuk bertahan dalam survival tersebut.</p>
                    <p>4. Administrasi : Lengkapi administrasi sebelum melakukan pendakian.</p>
                    <p>5. Melakukan briefing : Briefing dilakukan selambat-lambatnya satu hari sebelum hari pelaksanaan kegiatan gunung-hutan. Briefing dipimpin oleh pemimpin kegiatan dan dihadiri oleh seluruh anggota tim kegiatan gunung-hutan tersebut.</p>
                    <hr />
                    <h6 class="fw-bold">Prosedur Pendakian</h6>
                    <p>1. Anggota tim bergerak menurut komando dari leader.</p>
                    <p>2. Leader memutuskan setiap pergerakan berdasarkan kondisi tim dan kondisi yang ada di medan.
                    <p>3. Sweeper memastikan keutuhan dan kondisi seluruh anggota tim selama di perjalanan dan berkoordinasi dengan leader.
                    <p>4. Berjalan dengan kecepatan yang konsisten serta tidak terlalu cepat atau terlalu lambat.
                    <p>5. Memperhatikan langkah supaya tidak terlalu menghentak atau menyeret. Langkah kaki yang menghentak atau menyeret justru akan membutuhkan energy ekstra. Oleh karena itu, tetap berjalan dengan langkah kaki mantap namun tetap menapak ringan pada permukaan tanah.
                    <p>6. Tidak berlari ketika menemui jalan yang menurun</p>
                </div>

            </div>

        </div>
    </div>
    <div class="container-fluid text-center mb-5 mt-3">
        <a href="/Pages/ceklist" class="btn btn-primary btn-md">Lanjutkan Pendaftaran</a>
    </div>
    <?= $this->endSection(); ?>