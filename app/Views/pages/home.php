<!-- memanggil template.php untuk mewariskan method -->

<?= $this->extend('layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<style>
    p {
        font-size: 12px;
    }
</style>
<div class="container">
    <div class="col">
        <div class="row">
            <?php if (session()->getFlashdata('pesan')) : ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->getFlashdata('pesan'); ?>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>

<?= $this->include('layout/carousel'); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-5 col-md-12">
            <h1 class="text-center mt-3">Gunung Penanggungan</h1>
            <h5 class="text-center fw-bold text-success">Destinasi Wajib Pendaki</h5>

        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <div class="col-sm-5 col-md-10 text-center">

            <h6 class="text-center">Bila Anda termasuk pecinta alam sekaligus suka dengan perihal kebudayaan, Anda perlu sesekali mencoba berwisata ke Gunung Penanggungan. Di samping alamnya yang memikat, gunung yang tidak jauh dari pusat kebudayaan Majapahit itu juga sarat dengan beragam peninggalan budaya. Gunung setinggi 1.653 meter di atas permukaan laut ini terletak di dua kabupaten, yaitu Kabupaten Mojokerto dan Kabupaten Pasuruan. Ukuran gunung ini kecil dan sering disebut sebagai miniatur dari Gunung Semeru. Di hamparan puncak kedua gunung tersebut sama-sama terdapat pasir dan batuan yang luas.
            </h6>
        </div>
    </div>
</div>
</div>

<div class="container marketing mt-3">
    <div class="row">
        <div class="container justify-content-left col-md-12 bg-success">
            <h3 class="text-center" id="jalurpendakian">Jalur Pendakian</h3>
        </div>
    </div>
    <!-- Three columns of text below the carousel -->
    <div class="row ">
        <div class="col-lg-3 kotak ">
            <img class="rounded-circle mx-auto d-block" src="<?php echo base_url(); ?>/img/penanggungan1.jpg" alt="Generic placeholder image" width="140" height="140">
            <h2 class="text-center">Jalatunda</h2>
            <p class="text-center">Awal jalur ini adalah Petirtaan Jalatunda di Desa Seloliman, Kecamatan Trawas, yang berlokasi di sisi barat gunung. Jalur ini boleh dibilang "jalur sejarah" atau "jalur ziarah" karena banyak melewati objek-objek purbakala, seperti Candi Bayi, Candi Putri, Candi Pura, Candi Gentong, dan Candi Sinta. Ujung jalur ini adalah kawasan puncak sisi utara.
            </p>
            <div class="d-grid gap-2 d-md-flex justify-content-md-center">
                <input type="button" class="btn btn-outline-primary btn-sm" id="tampil1" value="Tampilkan Peta" />
                <a href="/imgadmin/imgjalur/Jolotundo.jpg" class="link-info fas fa-download" download="">Jalur Pendakian</a>
            </div>
            <iframe class="mt-1 img-fluid" id="peta1" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3954.337643782927!2d112.60304991405943!3d-7.646793877765313!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7877cf9f67c095%3A0xe87ed9c5cc81d3e7!2sPos%20Perijinan%20Gunung%20Penanggungan%20Via%20Tamiajeng!5e0!3m2!1sen!2sid!4v1637388080079!5m2!1sen!2sid" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            <div class="d-grid gap-2 d-md-flex justify-content-md-center">
                <input type="button" class="btn btn-outline-primary btn-sm justify-content-md-center" id="sembunyi1" value="Sembunyikan" />
            </div>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-3 kotak">
            <img class="rounded-circle mx-auto d-block" src="<?php echo base_url(); ?>/img/penanggungan2.jpg" alt="Generic placeholder image" width="140" height="140">
            <h2 class="text-center">Kedungudi</h2>
            <p class="text-center">Awal pendakian dimulai dari Desa Kedungudi, Kecamatan Trawas. Beberapa candi yang dilewati/berdekatan dengan jalur ini adalah Candi Guru dan Candi Siwa. Jalur ini juga berhubungan dengan jalur Jalatunda dan akan melewati Candi Sinta, Candi Lurah, Candi Carik, dan Candi Naga II.
            </p>
            <div class="d-grid gap-2 d-md-flex justify-content-md-center">
                <input type="button" class="btn btn-outline-primary btn-sm" id="tampil2" value="Tampilkan Peta" />
                <a href="/imgadmin/imgjalur/Kedungudi.jpg" class="link-info fas fa-download" download="">Jalur Pendakian</a>
            </div>
            <iframe class="mt-1 img-fluid" id="peta2" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15818.157744153155!2d112.5999391!3d-7.6249855!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8bd45e2562970796!2sPos%20pendakian%20Gunung%20Penanggungan%20via%20Desa%20Kedungudi!5e0!3m2!1sid!2sid!4v1656167406381!5m2!1sid!2sid" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            <div class="d-grid gap-2 d-md-flex justify-content-md-center">
                <input type="button" class="btn btn-outline-primary btn-sm justify-content-md-center" id="sembunyi2" value="Sembunyikan" />
            </div>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-3 kotak">
            <img class="rounded-circle mx-auto d-block" src="<?php echo base_url(); ?>/img/penanggungan3.jpg" alt="Generic placeholder image" width="140" height="140">
            <h2 class="text-center">Tamiajeng</h2>
            <p class="text-center">Jalur ini adalah jalur paling populer bagi pendaki, dimulai dari Desa Tamiajeng, Trawas, Kabupaten Mojokerto, yang merupakan sisi barat daya gunung. Jalur ini paling singkat, tetapi cukup terjal. Terdapat empat pos perhentian sebelum sampai lapangan puncak. Dari jalur ini akan melewati pelataran yang dikenal sebagai "Bukit Bayangan".
            </p>
            <div class="d-grid gap-2 d-md-flex justify-content-md-center">
                <input type="button" class="btn btn-outline-primary btn-sm" id="tampil3" value="Tampilkan Peta" />
                <a href="/imgadmin/imgjalur/Tamiajeng.jpg" class="link-info fas fa-download" download="">Jalur Pendakian</a>
            </div>
            <iframe class="mt-1 img-fluid" id="peta3" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3954.337502011395!2d112.60309441364228!3d-7.646809177764444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7877cf9f67c095%3A0xe87ed9c5cc81d3e7!2sPos%20Perijinan%20Gunung%20Penanggungan%20Via%20Tamiajeng!5e0!3m2!1sid!2sid!4v1656167715615!5m2!1sid!2sid" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            <div class="d-grid gap-2 d-md-flex justify-content-md-center">
                <input type="button" class="btn btn-outline-primary btn-sm justify-content-md-center" id="sembunyi3" value="Sembunyikan" />
            </div>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-3 kotak">
            <img class="rounded-circle mx-auto d-block" src="<?php echo base_url(); ?>/img/gunung3.jpg" alt="Generic placeholder image" width="140" height="140">
            <h2 class="text-center">Kesiman</h2>
            <p class="text-center">Berada di Dusun Sukoreno, Desa Kesiman, Kecamatan Prigen, Pasuruan. Jalur ini terkesan masih sepi, bahkan pada hari libur sekalipun. Jadi, sangat direkomendasikan bagi pendaki yang ingin mendaki gunung Penanggungan, melewati jalur ini, bagi yang ingin bercengkrama dengan suasana yang syahdu.
            </p>
            <div class="d-grid gap-2 d-md-flex justify-content-md-center">
                <input type="button" class="btn btn-outline-primary btn-sm" id="tampil4" value="Tampilkan Peta" />
                <a href="/imgadmin/imgjalur/Kesiman.jpg" class="link-info fas fa-download" download="">Jalur Pendakian</a>
            </div>
            <iframe class="mt-1 img-fluid" id="peta4" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3954.3833258566233!2d112.6427562136422!3d-7.641862277706332!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7d85de99eccdf%3A0xf0dc872128189397!2sPos%20Perijinan%20Gunung%20Penanggungan%20Jalur%20Kesiman!5e0!3m2!1sid!2sid!4v1656168029212!5m2!1sid!2sid" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            <div class="d-grid gap-2 d-md-flex justify-content-md-center">
                <input type="button" class="btn btn-outline-primary btn-sm justify-content-md-center" id="sembunyi4" value="Sembunyikan" />
            </div>
        </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->
</div>

<div class="container mt-3 ">
    <div class="row">
        <div class="container justify-content-left col-md-12 bg-success">
            <h3 class="text-center" id="beritainfo">Berita Penanggungan</h3>
        </div>
        <div class="container justify-content-left col-md-12">
            <h4>Berita Terkini</h4>
            <div class="col">
                <div class="row ">
                    <?php foreach ($berita as $b) : ?>
                        <div class="card mb-3 " style="max-width: 540px;">
                            <div class="row g-2">
                                <div class="col-md-4 mt-3">
                                    <img src="/imgadmin/imgberita/<?= $b['gambar']; ?>" class="img-fluid rounded-start " alt="...">
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h5 class="card-title"><?= $b['judul']; ?></h5>
                                        <h6 class="fw-light">No. <?= $b['id_berita']; ?></h6>
                                        <p class="card-text"><?= substr($b['deskripsi'], 0, 300) ?> <a href="/Page/Berita/detail/<?= $b['id_berita']; ?>">[Selengkapnya..]</a></p>
                                        <p class="card-text"><small class="text-muted">Dibuat : <?= $b['updated_at']; ?></small></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="row-md-12 text-center">
                    <?= $pager->links('berita', 'berita_pagination') ?>
                </div>

            </div>

        </div>



        <div class="container justify-content-left col-md-12 bg-success mt-3">
            <h3 class="text-center" id="alurbooking">Alur Booking</h3>
        </div>
        <div class="text-center ">
            <img src="/img/alurbooking.jpg" class="img-fluid rounded mb-5" alt="...">

        </div>
    </div>
</div>

<script>
    //Inisiasi awal penggunaan jQuery
    $(document).ready(function() {
        //Pertama sembunyikan elemen class gambar
        $('#peta1').hide();
        $('#peta2').hide();
        $('#peta3').hide();
        $('#peta4').hide();
        $('#sembunyi1').hide();
        $('#sembunyi2').hide();
        $('#sembunyi3').hide();
        $('#sembunyi4').hide();

        //Ketika elemen class tampil di klik maka elemen class gambar tampil
        $('#tampil1').click(function() {
            $('#peta1').show();
            $('#sembunyi1').show();
        });

        //Ketika elemen class sembunyi di klik maka elemen class gambar sembunyi
        $('#sembunyi1').click(function() {
            //Sembunyikan elemen class gambar
            $('#peta1').hide();
            $('#sembunyi1').hide();
        });
    });
</script>
<script>
    //Inisiasi awal penggunaan jQuery
    $(document).ready(function() {

        //Ketika elemen class tampil di klik maka elemen class gambar tampil
        $('#tampil2').click(function() {
            $('#peta2').show();
            $('#sembunyi2').show();
        });

        //Ketika elemen class sembunyi di klik maka elemen class gambar sembunyi
        $('#sembunyi2').click(function() {
            //Sembunyikan elemen class gambar
            $('#peta2').hide();
            $('#sembunyi2').hide();
        });
    });
</script>
<script>
    //Inisiasi awal penggunaan jQuery
    $(document).ready(function() {

        //Ketika elemen class tampil di klik maka elemen class gambar tampil
        $('#tampil3').click(function() {
            $('#peta3').show();
            $('#sembunyi3').show();
        });

        //Ketika elemen class sembunyi di klik maka elemen class gambar sembunyi
        $('#sembunyi3').click(function() {
            //Sembunyikan elemen class gambar
            $('#peta3').hide();
            $('#sembunyi3').hide();
        });
    });
</script>
<script>
    //Inisiasi awal penggunaan jQuery
    $(document).ready(function() {

        //Ketika elemen class tampil di klik maka elemen class gambar tampil
        $('#tampil4').click(function() {
            $('#peta4').show();
            $('#sembunyi4').show();
        });

        //Ketika elemen class sembunyi di klik maka elemen class gambar sembunyi
        $('#sembunyi4').click(function() {
            //Sembunyikan elemen class gambar
            $('#peta4').hide();
            $('#sembunyi4').hide();
        });
    });
</script>

<?= $this->endSection(); ?>