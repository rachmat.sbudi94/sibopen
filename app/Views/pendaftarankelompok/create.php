<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>

<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="my-2">Form Pendaftaran Pendakian</h2>
            <h5>Jadwal Pendakian</h5>
            <form action="/Pendaftaran/save" method="post" enctype="multipart/form-data">

                <?= csrf_field(); ?>
                <div class="form-group row">
                    <label for="text" class="col-sm-2 col-form-label">Mulai Pendakian</label>
                    <div class="col-sm-10">
                        <div class="bootstrap-iso">

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar">
                                    </i>
                                </div>
                                <input class="form-control" id="date" name="date" placeholder="DD/MM/YYYY" type="text" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="text" class="col-sm-2 col-form-label">Selesai Pendakian</label>
                    <div class="col-sm-10">
                        <div class="bootstrap-iso">

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar">
                                    </i>
                                </div>
                                <input class="form-control" id="date" name="date" placeholder="DD/MM/YYYY" type="text" />
                            </div>
                        </div>
                    </div>
                </div>

                <h5>Ketua Kelompok</h5>
                <div class="form-group row">
                    <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nama" name="nama" value="<?= old('nama'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="tgl_lahir" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="tgl_lahir" name="tgl_lahir" value="<?= old('tgl_lahir'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="jenis_kelamin" name="jenis_kelamin" value="<?= old('jenis_kelamin'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="jenis_identitas" class="col-sm-2 col-form-label">Jenis Identitas</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="jenis_identitas" name="jenis_identitas" value="<?= old('jenis_identitas'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="no_identitas" class="col-sm-2 col-form-label">No. Identitas</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="no_identitas" name="no_identitas" value="<?= old('no_identitas'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="kebangsaan" class="col-sm-2 col-form-label">Kebangsaan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="kebangsaan" name="kebangsaan" value="<?= old('kebangsaan'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="alamat" name="alamat" value="<?= old('alamat'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="pekerjaan" class="col-sm-2 col-form-label">Pekerjaan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="pekerjaan" name="pekerjaan" value="<?= old('pekerjaan'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="no_hp" class="col-sm-2 col-form-label">No. HP</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="no_hp" name="no_hp" value="<?= old('no_hp'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama_keluarga" class="col-sm-2 col-form-label">Nama Keluarga</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nama_keluarga" name="nama_keluarga" value="<?= old('nama_keluarga'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="hubungan" class="col-sm-2 col-form-label">Hubungan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="hubungan" name="hubungan" value="<?= old('hubungan'); ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="no_hpkeluarga" class="col-sm-2 col-form-label">No.HP Keluarga</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="no_hpkeluarga" name="no_hpkeluarga" value="<?= old('no_hpkeluarga'); ?>">
                    </div>
                </div>
        </div>
        <h6 class="text-primary text-center">*Pastikan Data yang tercantum sudah benar dan nomor HP terdaftar Whatsapp. Konfirmasi booking akan dikirim melalui Whatsapp</h6>
        <br>
        <h5>Anggota Kelompok</h5>
        <!-- <div class="form-group row">

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Registrasi</th>
                            <th scope="col">No Identitas</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Kwarganegaraan</th>
                            <th scope="col">Alamat</th>
                            <th scope="col">Pekerjaan</th>
                            <th scope="col">No. HP</th>
                            <th scope="col">Status</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($booking as $k) : ?>
                            <tr>
                                <th scope="row"><?= $i++; ?></th>
                                <td><?= $k['no_registrasi']; ?></td>
                                <td><?= $k['no_identitas']; ?></td>
                                <td><?= $k['nama']; ?></td>
                                <td><?= $k['kwarganegaraan']; ?></td>
                                <td><?= $k['alamat']; ?></td>
                                <td><?= $k['pekerjaan']; ?></td>
                                <td><?= $k['no_hp']; ?></td>
                                <td><?= $k['status']; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>


        </div>
        <div class="form-group row">
            <div class="d-grid col-2 mx-auto">
                <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#modalTambah" data-bs-whatever="@getbootstrap">Kelola Data</button>
            </div>
        </div> -->

        <br>

        </form>
        <h5>Konfirmasi Pendaftaran</h5>
        <h6 class="text-warning fw-bold text-center">Dengan menekan tombol daftar dibawah,maka Anda menyetujui segala persyaratan pendakian di Gunung Penanggungan.</h6>

        <div class="form-group row">
            <img src="/img/hargatiket.jpg" style="max-width:30%;" class="img-thumbnail col-sm-6" alt="...">

            <div class="col-sm-6">
                <h6 class="mt-5">* Harga tiket untuk 1 orang pendaki</h6>

            </div>
        </div>
    </div>
    <div class="d-grid gap-2 col-2 mx-auto mb-3">
        <button type="submit" class="btn btn-primary">Daftar</button>
        <button type="submit" class="btn btn-primary">Batal</button>
    </div>
</div>

<!-- <div class="modal fade" id="modalTambah" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Anggota</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <form action="/Pendaftaran/save" method="post">

                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">No Registrasi</label>
                        <div class="col-sm-9">
                            <input type="text" name="no_registrasi" class="form-control" placeholder="no_registrasi">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">No Identitas</label>
                        <div class="col-sm-9">
                            <input type="text" name="no_identitas" class="form-control" placeholder="no_identitas">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">Jenis Identitas</label>
                        <div class="col-sm-9">
                            <input type="text" name="jenis_identitas" class="form-control" placeholder="jenis_identitas">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">Nama</label>
                        <div class="col-sm-9">
                            <input type="text" name="nama" class="form-control" placeholder="nama">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">Kwarganegaraan</label>
                        <div class="col-sm-9">
                            <input type="text" name="kwarganegaraan" class="form-control" placeholder="kwarganegaraan">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">Alamat</label>
                        <div class="col-sm-9">
                            <input type="text" name="alamat" class="form-control" placeholder="alamat">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">Pekerjaan</label>
                        <div class="col-sm-9">
                            <input type="text" name="pekerjaan" class="form-control" placeholder="pekerjaan">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">No HP</label>
                        <div class="col-sm-9">
                            <input type="text" name="no_hp" class="form-control" placeholder="no_hp">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">Status</label>
                        <div class="col-sm-9">
                            <input type="text" name="status" class="form-control" placeholder="status">
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
        </div>
        </form>
    </div>
</div> -->

</div>

<?= $this->endSection(); ?>