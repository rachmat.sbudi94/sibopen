<nav class="navbar navbar-expand-lg navbar-dark bg-primary mt-3 ">
  <div class="container">
    <div class="row">
      <div class="col">
        <img src="/img/lmdh.jpg" alt="" width="80" height="80">

        <a class="navbar-brand ml-2 fst-italic " href="#">Informasi dan Booking Penanggungan</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

      </div>
    </div>
    <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-link active text-decoration-underline" href="/">Home <span class="sr-only">(current)</span></a>
        <a class="nav-link active text-decoration-underline" href="/pages/home.php#jalurpendakian">Jalur</a>
        <a class="nav-link active text-decoration-underline" href="/pages/home.php#beritainfo">Berita</a>
        <a class="nav-link active text-decoration-underline" href="/pages/home.php#alurbooking">Alur Booking</a>
        <a class="nav-link active text-decoration-underline" href="/pages/sop">SOP</a>
        <!-- <a class="nav-link" href="/pages/pendaftaran">Pendaftaran</a> -->
        <a class="nav-link active text-decoration-underline" href="/Admin/index">Login</a>
        <!-- <a class="nav-link active text-decoration-underline" href="/BookingPembayaran/tiket">Tiket</a> -->

        <!-- Jika mengakses lewat xampp menambahkan echo base_url seperti
    <a class="nav-link" href="tekan phpp ctrl+spasi hapus $ tulis base_url('/');">About</a>
    <a class="nav-link" href="tekan phpp ctrl+spasi hapus $ tulis base_url('/pages/about');">About</a>
        <a class="nav-link" href="tekan phpp ctrl+spasi hapus $ tulis base_url('/komik');">Komik</a>
 -->
        <form action="/BookingPembayaran/index" method="post">
          <div class="d-flex justify-content-end bg-success">
            <input type="text" class="form-control me-2" placeholder="Masukkan Nomor" name="keyword">
            <div class="input-group-append">
              <button class="btn btn-outline-light" type="submit" name="submit">Cari</button>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>


</nav>