<!-- memanggil template.php untuk mewariskan method -->
<?= $this->extend('layout/template'); ?>


<!-- memanggil content dari template.php -->
<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h2 class="my-3 mt-3 text-center">Pembayaran Booking</h2>
            <div class="card">
                <div class="card-body text-center fw-bold bg-warning">

                    <?php
                    if (empty($stats['status'])) {
                        echo '<div class="card-body text-center fw-bold bg-warning">' . 'Silahkan Melakukan Pembayaran' . '</div>';
                    } elseif ($stats['status'] == 'Belum Validasi') {
                        echo '<div class="card-body text-center fw-bold bg-success">' . 'Pembayaran Sudah Dilakukan, Menunggu Validasi Admin' . '</div>';
                    } else {
                        echo '<div class="card-body text-center fw-bold bg-light">' . 'Pembayaran Sudah diValidasi ' . "<a href='/exporttiket-pdf/" . $pembayaran['no_registrasi'] .  "'>Cetak Etiket</a>" . '</div>';
                    }
                    ?>

                </div>
            </div>

            <h4 class="">Batas pembayaran maksimal dalam 24 jam</h4>
            <h5 class="">* Lengkapi form</h5>
            <h5 class="">* Transfer ke Rekening BCA 82187642514 A/N LMDH Sumber Lestari</h5>


        </div>
    </div>
</div>
<div class="container">
    <div class="row mt-3">
        <div class="col-sm-5 col-md-8">

            <form action="/BookingPembayaran/save" method="post" enctype="multipart/form-data" class="row g-3">
                <div class="form-group row ">
                    <div class="col-md-6">
                        <label for="nama" class="form-label">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="<?= $pembayaran['nama']; ?>" readonly>
                    </div>
                    <div class="col-md-6">
                        <label for="no_identitas" class="form-label">No.Identitas</label>
                        <input type="text" class="form-control" id="no_identitas" name="no_identitas" value="<?= $pembayaran['no_identitas']; ?>" readonly>
                    </div>
                    <div class="col-md-6">
                        <label for="tgl_pendakian" class="form-label">Tanggal Pendakian</label>
                        <input type="text" class="form-control" id="tgl_pendakian" name="tgl_pendakian" value="<?= $pembayaran['tgl_pendakian']; ?>" readonly>
                    </div>
                    <div class="col-md-6">
                        <label for="no_registrasi" class="form-label">No.Registrasi</label>
                        <input type="text" class="form-control" id="no_registrasi" name="no_registrasi" value="<?= $pembayaran['no_registrasi']; ?>" readonly>
                    </div>
                    <div class="col-12">
                        <label for="jalur" class="form-label">Jalur</label>
                        <input type="text" class="form-control" id="jalur" name="jalur" value="<?= $pembayaran['jalur']; ?>" readonly>
                    </div>
                    <div class="col-12">
                        <label for="alamat" class="form-label">Alamat</label>
                        <input type="text" class="form-control" id="alamat" name="alamat" value="<?= $pembayaran['alamat']; ?>" readonly>
                    </div>
                    <div class="col-12">
                        <input type="hidden" class="form-control" id="totalbayar" name="totalbayar" value="<?= $total; ?>">
                    </div>
                    <div class="col-12">
                        <label for="buktipembayaran" class="form-label">Bukti Pembayaran</label>
                        <input class="form-control" id="bukti_pembayaran" name="bukti_pembayaran" type="file">
                    </div>

                    <hr class="mt-3">
                    <br>
                    <h6>Anggota Kelompok</h6>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama Anggota</th>
                                <th scope="col">No. Identitas</th>
                                <th scope="col">Jenis Kelamin</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1 + (10 * ($page - 1));
                            foreach ($anggota as $row) : ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $row['nama_anggota']; ?></td>
                                    <td><?= $row['no_identitasanggota']; ?></td>
                                    <td><?= $row['jenis_kelaminanggota']; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>


                    <div class="col-12">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="gridCheck">
                            <label class="form-check-label" for="gridCheck">
                                Data sudah benar
                            </label>
                        </div>

                    </div>
                </div>
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-primary btn-lg">Simpan</button>
                </div>
            </form>
        </div>
        <div class="col-sm-5 offset-sm-2 col-md-4 offset-md-0 text-center mt-3">
            <div class="card ">
                <div class="card-body ">
                    <h5 class="text-decoration-underline">Rincian Pembayaran</h5>
                    <h6>Anggota kelompok terdiri dari <?= $jumlah; ?> Orang</h6>
                    <h6>@<?= $jumlahtotal; ?> x Rp. 10000 (Termasuk Ketua)</h6>
                    <h5><b>Total Rp. <?= $total; ?></b></h5>
                </div>
            </div>
            <img src="/img/frame1.png" class="rounded mt-3" id="book" alt="...">
        </div>


    </div>
</div>

<?= $this->endSection(); ?>