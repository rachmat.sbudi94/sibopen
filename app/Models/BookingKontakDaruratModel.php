<?php

namespace App\Models;

use CodeIgniter\Model;

class BookingKontakDaruratModel extends Model
{
    protected $table = 'bookingkontakdarurat';
    protected $allowedFields = ['no_registrasi', 'nama_darurat1', 'tlp_darurat1', 'hubungan_darurat1', 'nama_darurat2', 'tlp_darurat2', 'hubungan_darurat2'];
    protected $returnType     = 'array';
    protected $useSoftDeletes = false;


    public function cariKontak($keyword = '')
    {
        if ($keyword == false) {
            return $this->findAll();
        }

        return $this->WHERE('no_registrasi', $keyword);
    }
}
