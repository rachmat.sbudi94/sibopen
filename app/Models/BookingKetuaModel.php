<?php

namespace App\Models;

use CodeIgniter\Model;

class BookingKetuaModel extends Model
{
    // protected $table = 'ketuabooking';
    protected $table = 'bookingketua';

    // protected $primaryKey = 'no_identitas';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['no_registrasi', 'jalur', 'tgl_pendakian', 'selesai_pendakian', 'nama', 'no_identitas', 'tgl_lahir', 'jenis_kelamin', 'kwarganegaraan', 'alamat', 'pekerjaan', 'no_hp'];



    public function getPendaftaran($no_registrasi = '')
    {
        if ($no_registrasi == false) {
            return $this->findAll();
        }
        return $this->like('no_registrasi', $no_registrasi)->first();
    }
    public function getKetua($keyword = '')
    {
        if ($keyword == false) {
            return $this->findAll();
        }
        return $this->like('no_registrasi', $keyword)->first();
    }
    public function getTanggalAnggota($tgl_pendakian = '')
    {
        return $this->table('bookingketua')
            ->join('bookinganggota', 'bookingketua.no_registrasi = bookinganggota.no_registrasi')
            ->where('tgl_pendakian', $tgl_pendakian);
    }
    public function getTanggalKetua($tgl_pendakian = '')
    {
        return $this->table('bookingketua')->where('tgl_pendakian', $tgl_pendakian);
    }

    // public function noPendaftaran()
    // {
    //     $conn = mysqli_connect('localhost', 'root', '', 'sibookingpenanggungan');
    //     $today = date('ymd');
    //     $char = 'PG' . $today;

    //     $query = mysqli_query($conn, "SELECT max(no_registrasi) as max_id FROM bookingketua WHERE no_registrasi LIKE '{$char}%' ORDER BY no_registrasi DESC LIMIT 1");
    //     // $query = $db->query('SELECT max(no_registrasi) as kodeTerbesar FROM booking');
    //     $result = mysqli_fetch_array($query);
    //     $getId = $result['max_id'];
    //     $no = substr($getId, -4, 4);
    //     $no++;
    //     // $no = $no + 1;
    //     // Oke next kita bakal generate kode otomatisnya
    //     // perintah sprintf("%04s", $no); digunakan untuk memformat string sebanyak 4 karakter
    //     // misal sprintf("%04s", 2); maka akan dihasilkan '0002'
    //     // atau misal sprintf("%04s", 1); maka akan dihasilkan string '0001'
    //     $no_pendaftaran = $char . sprintf("%04s", $no);
    //     return $no_pendaftaran;
    // }

    public function search($keyword)
    {
        // $builder = $this->table('orang');
        // $builder->like('nama', 'keyword');
        // return $builder;

        return $this->table('bookingketua')->like('nama', $keyword)->orLike('alamat', $keyword);
    }
}
