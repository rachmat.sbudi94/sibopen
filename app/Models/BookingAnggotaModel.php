<?php

namespace App\Models;

use CodeIgniter\Model;

class BookingAnggotaModel extends Model
{
    protected $table = 'bookinganggota';
    protected $allowedFields = ['no_registrasi', 'nama_anggota', 'no_tlpanggota', 'no_identitasanggota', 'kwarganegaraan_anggota', 'jenis_kelaminanggota'];
    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    public function cariAnggota($keyword = '')
    {
        if ($keyword == false) {
            return $this->findAll();
        }

        return $this->WHERE('no_registrasi', $keyword);
    }

    public function search($keyword = '')
    {
        // $builder = $this->table('orang');
        // $builder->like('nama', 'keyword');
        // return $builder;

        return $this->table('bookinganggota')->like('no_registrasi', $keyword);
    }
}
