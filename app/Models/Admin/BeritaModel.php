<?php

namespace App\Models\Admin;

use CodeIgniter\Model;

class BeritaModel extends Model
{
    protected $table = 'berita';

    protected $useTimestamps = true;

    protected $primaryKey = 'id_berita';

    //protected $useAutoIncrement = true;

    protected $allowedFields = ['id_berita', 'judul', 'kategori', 'deskripsi', 'gambar'];

    /*protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    */

    public function getBerita($id_berita = false)
    {
        if ($id_berita == false) {
            return $this->findAll();
        }
        return $this->where(['id_berita' => $id_berita])->first();
    }
    public function search($keyword)
    {
        // $builder = $this->table('orang');
        // $builder->like('nama', 'keyword');
        // return $builder;

        return $this->table('berita')->like('judul', $keyword)->orLike('kategori', $keyword);
    }
}
