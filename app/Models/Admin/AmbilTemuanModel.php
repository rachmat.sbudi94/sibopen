<?php

namespace App\Models\Admin;

use CodeIgniter\Model;

class AmbilTemuanModel extends Model
{
    protected $table = 'ambiltemuan';

    protected $primaryKey = 'id_ambiltemuan';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['id_ambiltemuan', 'nama_barang', 'deskripsi', 'no_identitas', 'alamat', 'no_hp', 'keterangan'];

    protected $useTimestamps = true;


    // public function getKehilangan($id_kehilangan = false)
    // {
    //     if ($id_kehilangan == false) {
    //         return $this->findAll();
    //     }
    //     return $this->where(['id_kehilangan' => $id_kehilangan])->first();
    // }
}
