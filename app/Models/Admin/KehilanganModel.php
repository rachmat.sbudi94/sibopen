<?php

namespace App\Models\Admin;

use CodeIgniter\Model;

class KehilanganModel extends Model
{
    protected $table = 'kehilangan';

    protected $primaryKey = 'id_kehilangan';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['id_kehilangan', 'nama_barang', 'deskripsi', 'no_identitas', 'alamat', 'no_hp', 'status_temuan'];

    protected $useTimestamps = true;


    public function getKehilangan($id_kehilangan = false)
    {
        if ($id_kehilangan == false) {
            return $this->findAll();
        }
        return $this->where(['id_kehilangan' => $id_kehilangan])->first();
    }
}
