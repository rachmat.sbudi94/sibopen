<?php

namespace App\Models;

use CodeIgniter\Model;

class TarifModel extends Model
{
    protected $table = 'tarif';

    protected $primaryKey = 'id_tarif';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['id_tarif', 'kategori', 'harga'];




    public function getTarif($id_tarif = false)
    {
        if ($id_tarif == false) {
            return $this->findAll();
        }
        return $this->where(['id_tarif' => $id_tarif])->first();
    }
}
