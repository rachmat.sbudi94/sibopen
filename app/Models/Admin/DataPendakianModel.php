<?php

namespace App\Models\Admin;

use CodeIgniter\Model;

class DataPendakianModel extends Model
{
    protected $table = 'datapendakian';
    protected $primaryKey = 'no_registrasi';
    protected $allowedFields = ['no_registrasi', 'tgl_cekin', 'tgl_cekout', 'cekout'];

    public function getSemuaDataPendakian()
    {
        $query =  $this->db->table('datapendakian')

            ->join('bookingketua', 'bookingketua.no_registrasi = datapendakian.no_registrasi', 'left')
            // ->join('bookinganggota', 'bookinganggota.no_registrasi = datapendakian.no_registrasi', 'left')
            ->get()->getResultArray();

        return $query;
    }
    public function getDataPendakianBerlangsung()
    {
        $cekout =  'Belum';
        $query = $this->table('datapendakian')->like('cekout', $cekout)
            ->join('bookingketua', 'bookingketua.no_registrasi = datapendakian.no_registrasi', 'left')
            ->get()->getResultArray();

        return $query;
    }
}
