<?php

namespace App\Models\Admin;

use CodeIgniter\Model;

class TemuanModel extends Model
{
    protected $table = 'Temuan';

    protected $primaryKey = 'id_temuan';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['id_temuan', 'nama_barang', 'deskripsi', 'keterangan', 'status_temuan'];

    protected $useTimestamps = true;


    public function getTemuan($id_temuan = false)
    {
        if ($id_temuan == false) {
            return $this->findAll();
        }
        return $this->where(['id_temuan' => $id_temuan])->first();
    }
}
