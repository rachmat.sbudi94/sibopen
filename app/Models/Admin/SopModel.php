<?php

namespace App\Models\Admin;

use CodeIgniter\Model;

class SopModel extends Model
{
    protected $table = 'sop';
    protected $primaryKey = 'id_sop';
    protected $useTimestamps = true;

    protected $allowedFields = ['id_sop', 'nama_sop'];

    public function getSop($id_sop = false)
    {
        if ($id_sop == false) {
            return $this->findAll();
        }
        return $this->where(['id_sop' => $id_sop])->first();
    }
}
