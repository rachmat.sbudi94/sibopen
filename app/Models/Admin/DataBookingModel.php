<?php

namespace App\Models\Admin;

use CodeIgniter\Model;

class DataBookingModel extends Model
{
    protected $table = 'bookinganggota';

    public function getPeriodeBooking($tgl_awal, $tgl_akhir)
    {
        $query = $this->db->table('bookinganggota')
            ->join('bookingketua', 'bookingketua.no_registrasi = bookinganggota.no_registrasi', 'right')
            ->join('bookingkontakdarurat', 'bookingkontakdarurat.no_registrasi = bookinganggota.no_registrasi', 'right')
            ->where('tgl_pendakian >=', $tgl_awal)
            ->where('tgl_pendakian <=', $tgl_akhir)
            ->get()->getResultArray();

        return $query;
    }

    public function getSimpanan()
    {
        $query =  $this->db->table('bookinganggota')

            ->join('bookingketua', 'bookingketua.no_registrasi = bookinganggota.no_registrasi', 'right')
            ->join('bookingkontakdarurat', 'bookingkontakdarurat.no_registrasi = bookinganggota.no_registrasi', 'right')
            ->get()->getResultArray();

        return $query;
    }
}
