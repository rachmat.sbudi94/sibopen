<?php

namespace App\Models\Admin;

use CodeIgniter\Model;

class ValidateModel extends Model
{
    protected $table = 'bookingvalidate';
    protected $primaryKey = 'no_registrasi';

    protected $allowedFields = ['no_registrasi', 'bukti', 'jumlah', 'updated_at', 'pic'];

    protected $useTimestamps = true;


    public function getValidate($no_registrasi = false)
    {
        if ($no_registrasi == false) {
            return $this->findAll();
        }
        return $this->where(['no_registrasi' => $no_registrasi])->first();
    }
    public function getSimpanan()
    {
        $query =  $this->db->table('bookinganggota')

            ->join('bookingketua', 'bookingketua.no_registrasi = bookinganggota.no_registrasi', 'right')
            ->join('bookingkontakdarurat', 'bookingkontakdarurat.no_registrasi = bookinganggota.no_registrasi', 'right')
            ->get()->getResultArray();

        return $query;
    }
}
