<?php

namespace App\Models\Admin;

use CodeIgniter\Model;

class KuotaModel extends Model
{
    protected $table = 'kuota';

    protected $primaryKey = 'id_kuota';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['id_kuota', 'kategori', 'nama_kuota', 'jumlah_kuota'];

    protected $useTimestamps = true;


    public function getKuota($status = false)
    {
        if ($status == false) {
            return $this->findAll();
        }
        return $this->where(['status' => $status])->findAll();
    }
    public function getHitKuota($status = '')
    {
        return $this->where(['status' => $status])->first();
    }
}
