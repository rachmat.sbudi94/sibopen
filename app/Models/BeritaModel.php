<?php

namespace App\Models;

use CodeIgniter\Model;

class BeritaModel extends Model
{
    protected $table = 'berita';

    protected $useTimestamps = true;

    protected $primaryKey = 'id_berita';

    //protected $useAutoIncrement = true;

    protected $allowedFields = ['id_berita', 'judul', 'kategori', 'deskripsi', 'gambar', 'created_at'];

    /*protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    */
    public function getBerita($id_berita = false)
    {
        if ($id_berita == false) {
            return $this->findAll();
        }
        return $this->where(['id_berita' => $id_berita])->first();
    }
}
