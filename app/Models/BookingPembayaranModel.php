<?php

namespace App\Models;

use CodeIgniter\Model;

class BookingPembayaranModel extends Model
{
    protected $table = 'bookingpembayaran';

    protected $primaryKey = 'no_registrasi';

    protected $useSoftDeletes = false;

    protected $allowedFields = ['no_registrasi', 'bukti_pembayaran', 'jumlah', 'status', 'updated_at', 'pic'];

    protected $useTimestamps = true;

    public function getPembayaran($status = '')
    {
        if ($status == false) {
            return $this->findAll();
        }
        return $this->table('bookingpembayaran')
            ->join('bookingketua', 'bookingketua.no_registrasi = bookingpembayaran.no_registrasi', 'right')
            ->like('status', $status)->findAll();
    }
    public function getPembayaranStatus($keyword = '')
    {

        $status = 'Validasi';
        return $this->table('bookingpembayaran')->like('status', $status)->like('no_registrasi', $keyword)->first();
    }
    public function getTiket($no_registrasi = '', $status = 'Sudah Validasi')
    {
        return $this->table('bookingpembayaran')->Where('no_registrasi', $no_registrasi)
            ->Where('alamat', $status)->findAll();
    }
    public function getStatus($keyword = '')
    {
        if ($keyword == false) {
            return $this->findAll();
        }
        return $this->like('no_registrasi', $keyword)->first();
    }
}
