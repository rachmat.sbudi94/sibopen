<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Pages::index');

$routes->get('/Kehilangan', 'Admin\Kehilangan::index');
$routes->get('/Kehilangan/create', 'Admin\Kehilangan::create');
$routes->get('/Kehilangan/save', 'Admin\Kehilangan::save');
$routes->get('/Kehilangan/edit/(:num)', 'Admin\Kehilangan::edit');
$routes->post('/Kehilangan/update/(:any)', 'Admin\Kehilangan::update/$1');

$routes->get('/Temuan', 'Admin\Temuan::index');
$routes->get('/Temuan/create', 'Admin\Temuan::create');
$routes->get('/Temuan/save', 'Admin\Temuan::save');
$routes->get('/Temuan/edit/(:num)', 'Admin\Temuan::edit');
$routes->post('/Temuan/update/(:any)', 'Admin\Temuan::update/$1');

$routes->get('/Admin/Kehilangan/delete/(:num)', 'Admin\Kehilangan::delete');
$routes->get('/Admin/Temuan/delete/(:num)', 'Admin\Temuan::delete');
$routes->get('/Admin/Berita/delete/(:any)', 'Admin\Berita::delete');

$routes->get('/Berita', 'Admin\Berita::index');
$routes->get('/Berita/(:any)', 'Admin\Berita::detail');
$routes->get('/Berita/create', 'Admin\Berita::create');
$routes->get('/Berita/save', 'Admin\Berita::save');
$routes->get('/Berita/edit/(:num)', 'Admin\Berita::edit');
$routes->get('/Berita/update/(:num)', 'Admin\Berita::update');
$routes->get('/Sop', 'Admin\Sop::index');
$routes->get('/Page/Berita/detail/(:any)', 'Berita::detail/$1');

$routes->get('/DetailPendakian/(:any)', 'Admin\CrDataBooking::anggota/$1');
$routes->get('/DetailBooking/(:any)', 'Admin\DataBooking::anggota/$1');

$routes->get('/Kuota/create', 'Admin\Kuota::create');
$routes->get('/Kuota/save', 'Admin\Kuota::save');
$routes->get('/Kuota/edit/(:any)', 'Admin\Kuota::edit');
$routes->get('/Admin/Kuota/update/(:any)', 'Admin\Kuota::update');
$routes->get('/Admin/Kuota/delete/(:any)', 'Admin\Kuota::delete');

$routes->get('/Admin/index', 'Admin\CrDataBooking::index', ['filter' => 'auth']);
$routes->get('/CrDataBooking/All', 'Admin\CrDataBooking::semuadatapendakian');
$routes->get('/CrDataBooking/Berlangsung', 'Admin\CrDataBooking::pendakianberlangsung');
$routes->post('/CrDataBooking/save', 'Admin\CrDataBooking::save');
$routes->post('/CrDataBooking/update/(:any)', 'Admin\CrDataBooking::updatependakianberlangsung/$1');

$routes->get('/AmbilTemuan', 'Admin\AmbilTemuan::index');

$routes->get('/Kuota/Stats', 'Kuota::hitungKuota');

$routes->post('/Validate/(:any)', 'Admin\Validate::update/$1');

// $routes->post('/Admin/AmbilTemuan/create/(:any)', 'Admin\AmbilTemuan::save/$1');
$routes->post('/PeriodeBooking', 'Admin\DataBooking::periode');

$routes->get('export-pdf', 'Admin\DataBooking::printpdfbooking');
$routes->post('exportperiodebooking-pdf', 'Admin\DataBooking::printpdfbookingperiode');
$routes->get('exporttiket-pdf/(:any)', 'BookingPembayaran::printpdfetiket/$1');

$routes->get('/loginadmin', 'Admin\Auth::index');
$routes->get('/registeradmin', 'Admin\Auth::register');



/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
