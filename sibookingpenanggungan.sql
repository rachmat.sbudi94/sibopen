-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2022 at 05:35 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sibookingpenanggungan`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id_berita` varchar(255) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id_berita`, `judul`, `kategori`, `deskripsi`, `gambar`, `created_at`, `updated_at`) VALUES
('3234', '4234', '23423', '<p>42wweewr3reeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee3</p>\r\n', 'default.jpg', '2022-06-02 02:53:54', '2022-06-02 02:53:54'),
('Baruy', 'e23', 'r23r', '<p>klklklklklk</p>\r\n', 'default.jpg', '2022-03-08 05:29:04', '2022-03-08 06:36:53'),
('drrtdt', 'g', 'dqw', '<p>gtughjbjhb</p>\r\n', 'gunung3.jpg', '2021-12-23 04:31:55', '2022-03-08 06:37:24'),
('e12e', 'e12e', 'e32e', '<p>e23ee2</p>\r\n', 'gunung2.jpg', '2022-03-09 20:30:51', '2022-03-09 20:30:51'),
('e3r', 'r4', 'r34', '<p>r34</p>\r\n', 'gunung2.jpg', '2022-03-08 06:06:49', '2022-03-08 06:06:49'),
('z', 'Conan Edo', '432', 'cftc', 'default.jpg', '2021-12-23 04:32:26', '2021-12-23 04:32:26');

-- --------------------------------------------------------

--
-- Table structure for table `bookinganggota`
--

CREATE TABLE `bookinganggota` (
  `no_registrasi` varchar(50) NOT NULL,
  `nama_anggota` varchar(50) NOT NULL,
  `no_tlpanggota` varchar(20) NOT NULL,
  `no_identitasanggota` varchar(20) NOT NULL,
  `kwarganegaraan_anggota` varchar(20) NOT NULL,
  `jenis_kelaminanggota` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bookinganggota`
--

INSERT INTO `bookinganggota` (`no_registrasi`, `nama_anggota`, `no_tlpanggota`, `no_identitasanggota`, `kwarganegaraan_anggota`, `jenis_kelaminanggota`) VALUES
('PG2206150001', 'Koli1', '1234', '3455', 'WNI', 'L'),
('PG2206150001', 'Koli2', '1234', '3455', 'WNA', 'P'),
('PG2206150001', 'Koli3', '1234', '23413123', 'WNA', 'P'),
('PG2206150002', 'w3w34', '1234', '3455', 'WNA', 'P'),
('PG2206150002', 'w3w342', '1234', '3455', 'WNA', 'L'),
('PG2206170001', '', '', '', 'Pilih...', 'Pilih...'),
('PG2206170003', '', '', '', 'Pilih...', 'Pilih...');

-- --------------------------------------------------------

--
-- Table structure for table `bookingketua`
--

CREATE TABLE `bookingketua` (
  `no_registrasi` varchar(30) NOT NULL,
  `jalur` varchar(20) NOT NULL,
  `tgl_pendakian` date NOT NULL,
  `selesai_pendakian` date NOT NULL,
  `nama` varchar(100) NOT NULL,
  `no_identitas` varchar(30) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `kwarganegaraan` varchar(20) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `no_hp` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bookingketua`
--

INSERT INTO `bookingketua` (`no_registrasi`, `jalur`, `tgl_pendakian`, `selesai_pendakian`, `nama`, `no_identitas`, `tgl_lahir`, `jenis_kelamin`, `kwarganegaraan`, `alamat`, `pekerjaan`, `no_hp`) VALUES
('PG2206150001', 'Daw', '2022-06-15', '2022-06-16', 'Suaib', '32424', '2022-06-29', 'L', 'WNI', '123', '234', '3123'),
('PG2206150002', 'sopo', '2022-06-30', '2022-06-16', 'Mauni', '32424', '0000-00-00', 'L', '123', '425', '3123', '423'),
('PG2206170001', 'Daw', '2022-06-29', '2022-06-03', 'Juki', '423', '2022-06-30', 'L', 'WNI', '425', '312', '4234'),
('PG2206170002', 'Daw', '2022-06-04', '2022-06-11', '23423', '123', '2022-07-01', 'L', '312', '3123', '123', '123'),
('PG2206170003', 'Daw1', '2022-06-11', '2022-07-01', 'tyyuu', '123', '2022-06-30', 'L', '423', '423', '111', '123'),
('PG2206170004', 'Daw1', '2022-06-16', '2022-06-02', 'rtyu', '24', '2022-06-03', 'P', '111', '425', '123', '312');

-- --------------------------------------------------------

--
-- Table structure for table `bookingkontakdarurat`
--

CREATE TABLE `bookingkontakdarurat` (
  `no_registrasi` varchar(30) NOT NULL,
  `nama_darurat1` varchar(50) NOT NULL,
  `tlp_darurat1` varchar(30) NOT NULL,
  `hubungan_darurat1` varchar(20) NOT NULL,
  `nama_darurat2` varchar(50) NOT NULL,
  `tlp_darurat2` varchar(30) NOT NULL,
  `hubungan_darurat2` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bookingkontakdarurat`
--

INSERT INTO `bookingkontakdarurat` (`no_registrasi`, `nama_darurat1`, `tlp_darurat1`, `hubungan_darurat1`, `nama_darurat2`, `tlp_darurat2`, `hubungan_darurat2`) VALUES
('PG2206150001', 'Yoki1', '97896', 'Saudara', 'Yoki2', '21412', 'Saudara'),
('PG2206150002', 'KDKulo1', '97896', 'Saudara', 'KDKulo2', '21412', 'Anak'),
('PG2206170001', 'Yoki', '97896', 'Suami', 'Yoki2', '21412', 'Orang Tua'),
('PG2206170002', 'Yoki', '97896', 'Suami', 'Yoki2', '21412', 'Suami'),
('PG2206170003', 'Yoki', '97896', 'Orang Tua', 'Yoki2', '21412', 'Anak'),
('PG2206170004', 'Yoki', '97896', 'Suami', 'Yoki2', '21412', 'Orang Tua');

-- --------------------------------------------------------

--
-- Table structure for table `bookingpembayaran`
--

CREATE TABLE `bookingpembayaran` (
  `no_registrasi` varchar(30) NOT NULL,
  `bukti_pembayaran` varchar(255) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `pic` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bookingpembayaran`
--

INSERT INTO `bookingpembayaran` (`no_registrasi`, `bukti_pembayaran`, `jumlah`, `created_at`, `updated_at`, `status`, `pic`) VALUES
('PG2206150001', '1655274502_ba11b9f3939f2373d46b.jpg', 40000, '2022-06-14 23:58:22', '2022-06-19 07:52:08', 'Sudah Validasi', 'Komang'),
('PG2206150002', '1655279034_c54d04440dbe662cebb6.jpg', 30000, '2022-06-14 19:43:54', '2022-06-19 00:32:53', 'Sudah Validasi', 'Komang');

-- --------------------------------------------------------

--
-- Table structure for table `bookingvalidate`
--

CREATE TABLE `bookingvalidate` (
  `no_registrasi` varchar(50) NOT NULL,
  `bukti` varchar(50) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(10) NOT NULL,
  `pic` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bookingvalidate`
--

INSERT INTO `bookingvalidate` (`no_registrasi`, `bukti`, `jumlah`, `created_at`, `updated_at`, `status`, `pic`) VALUES
('', '', 0, '2022-06-14 22:41:04', '2022-06-14 22:41:04', 0, 'Komang');

-- --------------------------------------------------------

--
-- Table structure for table `cobaketua`
--

CREATE TABLE `cobaketua` (
  `no_registrasi` varchar(30) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cobaketua`
--

INSERT INTO `cobaketua` (`no_registrasi`, `nama`) VALUES
('PG2206040010', 'yuhu'),
('PG2206040011', '4444');

-- --------------------------------------------------------

--
-- Table structure for table `kehilangan`
--

CREATE TABLE `kehilangan` (
  `id_kehilangan` varchar(20) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `no_identitas` varchar(20) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_hp` varchar(50) NOT NULL,
  `status_temuan` varchar(20) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kehilangan`
--

INSERT INTO `kehilangan` (`id_kehilangan`, `nama_barang`, `deskripsi`, `no_identitas`, `alamat`, `no_hp`, `status_temuan`, `created_at`, `updated_at`) VALUES
('1', 'Carrier', '<p>Johannes Leimena&nbsp;&ndash;&nbsp;<a href=\"https://id.wikipedia.org/wiki/Rahmah_El_Yunusiyah\">Rahmah El Yunusiyah</a>&nbsp;&ndash;&nbsp;<a href=\"https://id.wikipedia.org/wiki/NASCAR_Seri_Piala\">NASCAR Seri Piala</a></p>\r\n\r\n<p>&nbsp;</p>\r\n', '4153215', 'Surabaya', '2653267', 'Belum', '2022-03-10 01:05:15', '2022-03-09 20:38:28'),
('1231', 'Sleeping Bag', '<p>hftyf</p>\r\n', '32424', '423', '3123', 'Belum', '2022-03-09 20:22:03', '2022-03-09 20:22:03'),
('23', '123', '<p>dqwd</p>\r\n', '24', '123', '4234', 'Belum', '2022-03-09 19:45:23', '2022-03-09 19:47:59'),
('231231231', '31231', '<p>i sering kejadian&nbsp;ketika data diinput lebih lebar dari lebar kolom terdefinisi di struktur.<br />\r\nMysql tidak memperdulikan, jadi data langsung tersimpan sekalipun &nbsp;terpotong. Kasus pernah terjadi misal alamat email didefinisikan 20, ternyata ada yg panjang pisan sehingga misal nama_orang_panjang@gma (gmail.com nya terpotong).</p>\r\n', 'e12', '12e', 'e12', 'Belum', '2022-03-09 20:29:00', '2022-03-09 20:29:00'),
('3211', 'Tas', '<p>few</p>\r\n', '123', '3123', '3123', '3123', '2022-06-02 01:54:11', '2022-06-02 01:54:11');

-- --------------------------------------------------------

--
-- Table structure for table `kuota`
--

CREATE TABLE `kuota` (
  `id_kuota` varchar(255) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `nama_kuota` varchar(50) NOT NULL,
  `jumlah_kuota` int(10) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kuota`
--

INSERT INTO `kuota` (`id_kuota`, `kategori`, `nama_kuota`, `jumlah_kuota`, `created_at`, `updated_at`, `status`) VALUES
('Normal1', 'Normal', 'Hari Biasa jeh', 2000, '2022-03-08 09:01:48', '2022-06-02 03:26:50', 'non'),
('Normal2', 'Liburan', 'Liburan', 1000, '2022-03-08 02:52:06', '2022-03-08 04:43:32', 'aktif');

-- --------------------------------------------------------

--
-- Table structure for table `sop`
--

CREATE TABLE `sop` (
  `id_sop` int(2) NOT NULL,
  `nama_sop` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sop`
--

INSERT INTO `sop` (`id_sop`, `nama_sop`, `created_at`, `updated_at`) VALUES
(1, 'ISMAIL.pdf', '2022-04-02 11:41:55', '2022-04-02 11:41:55');

-- --------------------------------------------------------

--
-- Table structure for table `tarif`
--

CREATE TABLE `tarif` (
  `id_tarif` varchar(50) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tarif`
--

INSERT INTO `tarif` (`id_tarif`, `kategori`, `harga`) VALUES
('Tarif1', 'WNA', 15000),
('Tarif2', 'Normal', 10000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `bookingketua`
--
ALTER TABLE `bookingketua`
  ADD PRIMARY KEY (`no_registrasi`);

--
-- Indexes for table `bookingkontakdarurat`
--
ALTER TABLE `bookingkontakdarurat`
  ADD PRIMARY KEY (`no_registrasi`);

--
-- Indexes for table `bookingpembayaran`
--
ALTER TABLE `bookingpembayaran`
  ADD PRIMARY KEY (`no_registrasi`);

--
-- Indexes for table `kehilangan`
--
ALTER TABLE `kehilangan`
  ADD PRIMARY KEY (`id_kehilangan`);

--
-- Indexes for table `kuota`
--
ALTER TABLE `kuota`
  ADD PRIMARY KEY (`id_kuota`);

--
-- Indexes for table `sop`
--
ALTER TABLE `sop`
  ADD PRIMARY KEY (`id_sop`);

--
-- Indexes for table `tarif`
--
ALTER TABLE `tarif`
  ADD PRIMARY KEY (`id_tarif`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
